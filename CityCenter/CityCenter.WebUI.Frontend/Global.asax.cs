﻿using System;
using System.Security.Principal;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web;
using CityCenter.BLL.Abstract;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Controllers;

namespace CityCenter.WebUI.Frontend
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            UnityConfig.RegisterComponents();
            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DependencyResolver.Current.GetService<IAutoDeclineService>().StartService();
        }

        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            if (FormsAuthentication.CookiesSupported)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        var formsAuthenticationTicket =
                            FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value);
                        if (formsAuthenticationTicket != null)
                        {
                            var username = formsAuthenticationTicket.Name;
                            var securityManager = DependencyResolver.Current.GetService<ISecurityManager>();
                            bool status;
                            var roles = securityManager.ReadUserRole(username, out status);

                            if (!status)
                            {
                                securityManager.SignOut();
                            }
                            else
                            {
                                HttpContext.Current.User = new GenericPrincipal(
                                    new GenericIdentity(username, "Forms"), roles.Split(';'));
                            }
                        }
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var exception = Server.GetLastError();
            var httpException = exception as HttpException;
            if (httpException != null && httpException.GetHttpCode() == 404)
            {
                var routeData = new RouteData();
                routeData.Values.Add("controller", "Error404");
                routeData.Values.Add("action", "Error404");

                Server.ClearError();

                Response.TrySkipIisCustomErrors = true;
                HttpContext.Current.Response.ContentType = "text/html; charset=UTF-8";
                IController errorController = new Error404Controller();
                errorController.Execute(new RequestContext(
                    new HttpContextWrapper(Context), routeData));
            }
        }
    }
}