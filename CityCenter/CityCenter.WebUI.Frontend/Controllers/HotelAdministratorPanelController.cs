﻿using System;
using System.Web;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Code;
using CityCenter.WebUI.Frontend.Models.Helper;
using CityCenter.WebUI.Frontend.Models.Hotel;
using CityCenter.WebUI.Frontend.Models.Panel;
using PagedList;

namespace CityCenter.WebUI.Frontend.Controllers
{
    [AuthorizeOrThrow404("HotelAdmin")]
    public class HotelAdministratorPanelController : Controller
    {
        #region Fields

        private readonly IUsersManager _usersManager;
        private readonly IHotelsManager _hotelsManager;
        private readonly ISecurityManager _securityManager;
        private readonly IEmailManager _emailManager;

        #endregion

        #region Constructor

        public HotelAdministratorPanelController(IUsersManager usersManager, IHotelsManager hotelsManager, ISecurityManager securityManager, IEmailManager emailManager)
        {
            _usersManager = usersManager;
            _hotelsManager = hotelsManager;
            _securityManager = securityManager;
            _emailManager = emailManager;
        }

        #endregion

        #region WebActions

        [HttpGet]
        public ActionResult Panel()
        {
            return View();
        }

        [HttpGet]
        public ActionResult EditHotelContent(int hotelId)
        {
            #region Validation

            var userId = _securityManager.GetCurrentUserId();
            if (!_securityManager.IsUserAdministratorOfHotel(userId, hotelId))
            {
                throw new HttpException(404, "HTTP/1.1 404 Not Found");
            }

            #endregion

            var hotelDiscriptionModel = new HotelDescriptionModel
            {
                HotelName = _hotelsManager.GetHotelById(hotelId).Name,
                HotelDescription = _hotelsManager.GetHotelDescriptionById(hotelId)
            };

            ViewBag.CountOfPendingReservation = _hotelsManager.GetPendingReservationCountByHotelId(hotelId);

            return PartialView("_EditHotelContent", hotelDiscriptionModel);
        }
        
        [HttpPost]
        public ActionResult EditHotelContent(HotelDescriptionModel model)
        {
            if (ModelState.IsValid)
            {
                _hotelsManager.UpdateHotelDescription(model.HotelDescription);
                _hotelsManager.UpdateHotelName(model.HotelName, model.HotelDescription.HotelId);
                return RedirectToAction("Panel");
            }
            return PartialView("_EditHotelContent");
        }

        [HttpGet]
        public FileContentResult GetHotelImage(int hotelId)
        {
            var desc = _hotelsManager.GetHotelDescriptionById(hotelId);
            return File(desc.Photo, MimeMapping.GetMimeMapping(desc.PhotoExtension));
        }

        [HttpPost]
        public ActionResult UploadNewImage(int hotelId, HttpPostedFileBase image)
        {
            if (image != null)
            {
                var hotelDescription = _hotelsManager.GetHotelDescriptionById(hotelId);
                hotelDescription.Photo = new byte[image.ContentLength];
                hotelDescription.PhotoExtension = image.ContentType;
                image.InputStream.Read(hotelDescription.Photo, 0, image.ContentLength);
                _hotelsManager.UpdateHotelImage(hotelDescription);
            }
            return RedirectToAction("EditHotelContent", new { hotelId });
        }

        [HttpGet]
        public ActionResult PendingReservations(int hotelId, int page = 1)
        {
            #region Validation

            var userId = _securityManager.GetCurrentUserId();

            if (!_securityManager.IsUserAdministratorOfHotel(userId, hotelId))
            {
                throw new HttpException(404, "HTTP/1.1 404 Not Found");
            }

            #endregion

            var pagedListModel = new PagedListModel()
            {
                Page = page,
                PageSize = 2,
                TotalItems = _hotelsManager.GetPendingReservationCountByHotelId(hotelId)
            };

            ViewBag.TotalItems = pagedListModel.TotalItems;
            ViewBag.PageSize = pagedListModel.PageSize;
            ViewBag.CountOfPendingReservation = pagedListModel.TotalItems;

            #region Paging Validation

            if (page < 1)
            {
                return RedirectToAction("PendingReservations");
            }
            if (page != 1 && (page - 1) * pagedListModel.PageSize >= pagedListModel.TotalItems)
            {
                return RedirectToAction("PendingReservations", new
                {
                    page = (int)Math.Ceiling((double)pagedListModel.TotalItems / pagedListModel.PageSize)
                });
            }

            #endregion

            var reservationEntities = _hotelsManager.GetPendingHotelReservationByHotelId(hotelId, pagedListModel.Page,
                pagedListModel.PageSize);

            var pagedList = new StaticPagedList<HotelReservationDetails>(reservationEntities, page, pagedListModel.PageSize,
                pagedListModel.TotalItems);

            var model = new AdminHotelReservationDetailsModel()
            {
                ReservationDetails = pagedList,
                HotelId = hotelId
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult AcceptedReservations(int hotelId, int page = 1)
        {
            #region Validation

            var userId = _securityManager.GetCurrentUserId();

            if (!_securityManager.IsUserAdministratorOfHotel(userId, hotelId))
            {
                throw new HttpException(404, "HTTP/1.1 404 Not Found");
            }

            #endregion

            var pagedListModel = new PagedListModel()
            {
                Page = page,
                PageSize = 3,
                TotalItems = _hotelsManager.GetReservationCountByHotelId(hotelId)
            };

            ViewBag.TotalItems = pagedListModel.TotalItems;
            ViewBag.PageSize = pagedListModel.PageSize;
            ViewBag.CountOfPendingReservation = _hotelsManager.GetPendingReservationCountByHotelId(hotelId);

            #region Paging Validation

            if (page < 1)
            {
                return RedirectToAction("PendingReservations");
            }
            if (page != 1 && (page - 1) * pagedListModel.PageSize >= pagedListModel.TotalItems)
            {
                return RedirectToAction("PendingReservations", new { page = (int)Math.Ceiling((double)pagedListModel.TotalItems / pagedListModel.PageSize) });
            }

            #endregion

            var reservationEntities = _hotelsManager.GetHotelReservationByHotelId(hotelId, pagedListModel.Page,
                pagedListModel.PageSize);

            var pagedList = new StaticPagedList<HotelReservationDetails>(reservationEntities, page, pagedListModel.PageSize,
                pagedListModel.TotalItems);

            var model = new AdminHotelReservationDetailsModel()
            {
                ReservationDetails = pagedList,
                HotelId = hotelId
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult AcceptBook(int reservationId, int hotelId)
        {
            var hotelName = _hotelsManager.GetHotelById(hotelId).Name;
            _hotelsManager.AcceptReservation(reservationId);
            SendEmailReservationNotification(hotelName, reservationId, reservationStatus: "Accepted");
            return RedirectToAction("PendingReservations", new { hotelId });
        }

        [HttpPost]
        public ActionResult DeclineBook(int reservationId, int hotelId)
        {
            var hotelName = _hotelsManager.GetHotelById(hotelId).Name;
            _hotelsManager.DeclineReservation(reservationId);
            SendEmailReservationNotification(hotelName, reservationId, reservationStatus: "Declined");
            return RedirectToAction("PendingReservations", new { hotelId });
        }

        #endregion

        #region Helper

        private void SendEmailReservationNotification(string hotelName, int reservationId, string reservationStatus)
        {
            var userId = _usersManager.GetUserIdByReservationId(reservationId);
            var user = _usersManager.GetUserById(userId);

            var mailSubject = "Reservation Notification";
            var mailBody = string.Format(
                "<div style=\"font-size:16px;\">Dear {0} {1}.<br/>"
                + "Your Reservation at hotel {2} was : {3}",
                user.FirstName, user.Surname, hotelName, reservationStatus);

            _emailManager.SendEmail(user.Email, mailSubject, mailBody);
        }

        #endregion
    }
}