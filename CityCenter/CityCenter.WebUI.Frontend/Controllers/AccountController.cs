﻿using System;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using System.Web;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Models.Account;

namespace CityCenter.WebUI.Frontend.Controllers
{
    public class AccountController : Controller
    {
        #region Fields

        private readonly IUsersManager _userManager;

        private readonly ISecurityManager _securityManager;

        private readonly IEmailManager _emailManager;

        #endregion

        #region Constructor

        public AccountController(IUsersManager userManager, ISecurityManager securityManager, IEmailManager emailManager)
        {
            _userManager = userManager;
            _securityManager = securityManager;
            _emailManager = emailManager;
        }

        #endregion

        #region WebActions

        [HttpGet]
        public ActionResult LogIn(string returnUrl)
        {
            #region Validation

            if (_securityManager.IsAuthenticated())
            {
                return RedirectToAction("Index", "MainPage");
            }

            #endregion

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        public ActionResult LogIn(UserLogInModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (_securityManager.Authentication(model.Login, model.Password))
                {
                    return Redirect(returnUrl ?? Url.Action("Index", "MainPage"));
                }
                ModelState.AddModelError("", "Invalid login or password");
                return View(model);
            }
            return View(model);
        }

        [Authorize]
        [HttpGet]
        public ActionResult LogOut()
        {
            _securityManager.SignOut();
            return RedirectToAction("Index", "MainPage");
        }

        [HttpGet]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(RestorePasswordModel model)
        {
            var user = _userManager.GetUserByEmail(model.Email);
            _securityManager.PasswordRecovery(user);
            SendPassword(user);
            return RedirectToAction("Index", "MainPage");
        }

        [HttpGet]
        public ActionResult ChangePassword(string token, string email)
        {
            #region Validation
            if (token == string.Empty || email == string.Empty)
            {
                throw new HttpException(404, "invalid data");
            }

            #endregion

            var user = _userManager.GetUserByEmail(email);
            if (user == null || token != _userManager.GetUserPasswordRecoveryToken(user.Id))
            {
                throw new HttpException(404, "invalid data");
            }
            var model = new PasswordRecoveryModel()
            {
                UserId = user.Id
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult ChangePassword(PasswordRecoveryModel model)
        {
            var user = _userManager.GetUserById(model.UserId);
            user.Password = model.NewPassword;
            _securityManager.UpdateUserPassword(user);
            _userManager.DeletePasswordRecoveryToken(user.Id);
            return RedirectToAction("Index", "MainPage");
        }

        [HttpGet]
        public ActionResult SignUp()
        {
            #region Validation

            if (_securityManager.IsAuthenticated())
            {
                return RedirectToAction("Index", "MainPage");
            }

            #endregion

            return View();
        }

        [HttpPost]
        public ActionResult SignUpSubmit(UserSignUpModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    Login = model.Login,
                    FirstName = model.FirstName,
                    Surname = model.Surname,
                    Email = model.Email,
                    Phone = model.Phone,
                    Password = model.Password
                };
                user.Id = _securityManager.RegisterNewUser(user);
                SendSignUpEmail(user);
                return View("SignUpConfirm");
            }
            return View("SignUp");
        }

        [HttpGet]
        public ActionResult SignUpComplete(string token, string email)
        {
            #region Validation

            if (token == string.Empty || email == string.Empty)
            {
                throw new HttpException(404, "invalid data");
            }

            #endregion

            var user = _userManager.GetUserByEmail(email);
            if (user == null || token != _userManager.GetUserRegistrationToken(user.Id))
            {
                throw new HttpException(404, "invalid data");
            }
            _userManager.UserConfirmRegistration(user.Id);
            return View("SignUpComplete");
        }

        public ActionResult SignUpConfirm()
        {
            return View();
        }

        #endregion

        #region Validation

        [HttpGet]
        public JsonResult IsLoginValid(string login)
        {
            var user = _userManager.GetUserByLogin(login);
            return Json(user == null || !string.Equals(user.Login, login, StringComparison.CurrentCultureIgnoreCase), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult IsEmailValid(string email)
        {
            var user = _userManager.GetUserByEmail(email);
            return Json(user == null || user.Email != email, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult IsEmailExist(string email)
        {
            var user = _userManager.GetUserByEmail(email);
            return Json(user != null && user.Email == email, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult IsPhoneValid(string phone)
        {
            var user = _userManager.GetUserByPhone(phone);
            return Json(user == null || user.Phone != phone, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Helper

        private void SendPassword(User user)
        {
            if (Request.Url != null)
            {
                var mailSubject = "Restore password";
                var mailBody = string.Format(
                    "<div style=\"font-size:16px;\">Dear {0} {1}.<br/>"+
                    "In order to change password " +
                    "<a href=\"{2}\" style=\"font-size:14px; color:blue; text-decoration: underline;\" title=\"Change password\">Click here</a><br/>"+
                    "<br/> Best regarts, <br/>Lviv City Center Team</div>",
                    user.FirstName, user.Surname,
                    Url.Action("ChangePassword", "Account",
                        new { Token = _userManager.GetUserPasswordRecoveryToken(user.Id), user.Email },
                        Request.Url.Scheme));

                _emailManager.SendEmail(user.Email, mailSubject, mailBody);
            }
        }

        private void SendSignUpEmail(User user)
        {
            if (Request.Url != null)
            {
                var mailSubject = "Email confirmation";
                var mailBody = string.Format(
                    "<div style=\"font-size:16px;\">Dear {0} {1},<br/>"
                    +
                    " Welcome to Lviv City Center!<br/>In order to complete your registation, " +
                    "please verify your email address by following the link<br/><br/>" +
                    "<a href=\"{2}\" style=\"font-size:18px; color:blue; text-decoration: underline;\" title=\"User Email Confirm\">Click here</a><br/>" +
                    "<br/> Best regarts, <br/>Lviv City Center Team</div>",
                    user.FirstName, user.Surname,
                    Url.Action("SignUpComplete", "Account",
                        new { Token = _userManager.GetUserRegistrationToken(user.Id), user.Email },
                        Request.Url.Scheme));

                _emailManager.SendEmail(user.Email, mailSubject, mailBody);
            }
        }

        #endregion
    }
}
