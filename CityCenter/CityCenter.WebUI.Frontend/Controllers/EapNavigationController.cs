﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Models.Panel;

namespace CityCenter.WebUI.Frontend.Controllers
{
    public class EapNavigationController : Controller
    {
        #region Fields

        private readonly IUsersManager _userManager;
        private readonly IExcursionsManager _excursionsManager;
        private readonly ISecurityManager _securityManager;

        #endregion

        #region Constructor

        public EapNavigationController(IUsersManager usersManager, IExcursionsManager excursionsManager, ISecurityManager securityManager)
        {
            _userManager = usersManager;
            _excursionsManager = excursionsManager;
            _securityManager = securityManager;
        }

        #endregion

        #region WebActions

        [HttpGet]
        [ChildActionOnly]
        public ActionResult Menu()
        {
            var userId = _securityManager.GetCurrentUserId();
            var model = new ExcursionAdministratorModel()
            {
                UserRoles = _userManager.GetUserRoles(userId),
                CurrentExcursion = ConvertToSelectListItem(),
            };

            return PartialView("_ExcursionMenu", model);
        }

        #endregion

        #region Helper

        private List<SelectListItem> ConvertToSelectListItem()
        {
            var userId = _securityManager.GetCurrentUserId();
            var excursion = _excursionsManager.GetExcursionsByAdminId(userId);
            return excursion.Select(e => new SelectListItem() { Text = e.Name, Value = e.Id.ToString() }).ToList();
        }

        #endregion
    }
}