﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.WebUI.Frontend.Models.Excursion;
using CityCenter.WebUI.Frontend.Models.Helper;
using PagedList;

namespace CityCenter.WebUI.Frontend.Controllers
{
    public class ExcursionsController : Controller
    {
        #region Fields

        private readonly IExcursionsManager _excursionsManager;

        #endregion

        #region Constructor

        public ExcursionsController(IExcursionsManager excursionsManager)
        {
            _excursionsManager = excursionsManager;
        }

        #endregion

        #region WebActions

        [HttpGet]
        public ActionResult Excursions(int page = 1)
        {
            var pagedListModel = new PagedListModel()
            {
                Page = page,
                PageSize = 2,
                TotalItems = _excursionsManager.GetExcursionDescriptionsCount()
            };

            #region Validation

            if (page < 1)
            {
                return RedirectToAction("Excursions");
            }
            if ((page != 1) &&
                ((page - 1) * pagedListModel.PageSize >= pagedListModel.TotalItems))
            {
                return RedirectToAction("Excursions", new { page = (int)Math.Ceiling((double)pagedListModel.TotalItems / pagedListModel.PageSize) });
            }

            #endregion

            var entities = _excursionsManager.GetExcursionDescriptions(pagedListModel.Page, pagedListModel.PageSize);
            var excursions = entities.Select(item => new ExcursionDescriptionModel
            {
                Description = item,
                Name = _excursionsManager.GetExcursionById(item.ExcursionId).Name,
                Excursion = _excursionsManager.GetExcursionById(item.ExcursionId)
            }).ToList();

            var pagedList = new StaticPagedList<ExcursionDescriptionModel>(excursions, page, pagedListModel.PageSize, pagedListModel.TotalItems);

            return View(pagedList);
        }

        [HttpGet]
        public FileContentResult GetExcursionImage(int excursionId)
        {
            var desc = _excursionsManager.GetExcursionDescriptionById(excursionId);
            return File(desc.Photo, MimeMapping.GetMimeMapping(desc.PhotoExtension));
        }

        [HttpGet]
        public ActionResult ExcursionInfo(int excursionId)
        {
            var excursion = _excursionsManager.GetExcursionById(excursionId);
            var excursions = _excursionsManager.GetExcursionDescriptionById(excursionId);
            var model = new ExcursionDescriptionModel()
            {
                Name = excursion.Name,
                Description = excursions,
                Excursion = _excursionsManager.GetExcursionById(excursionId)
            };
            
            return View(model);
        }

        #endregion
    }
}