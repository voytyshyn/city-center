﻿using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.WebUI.Frontend.Models;
using CityCenter.WebUI.Frontend.Models.Account;

namespace CityCenter.WebUI.Frontend.Controllers
{
    public class MainPageController : Controller
    {
        #region Fields

        private readonly IUsersManager _userManager;

        #endregion

        #region Constructor

        public MainPageController(IUsersManager userManager)
        {
            _userManager = userManager;
        }

        #endregion

        #region WebActions

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Map()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Feedback()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Feedback(FeedbackModel model)
        {
            if (ModelState.IsValid)
            {                
                _userManager.InsertUserFeedback(model.Login, model.Email, model.Message);
                return View("Index");
            }
            return View("Feedback");
        }

        #endregion
    }
}
