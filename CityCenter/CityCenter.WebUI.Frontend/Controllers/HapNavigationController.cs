﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Models.Panel;

namespace CityCenter.WebUI.Frontend.Controllers
{
    public class HapNavigationController : Controller
    {
        #region Fields

        private readonly IUsersManager _userManager;
        private readonly IHotelsManager _hotelManager;
        private readonly ISecurityManager _securityManager;

        #endregion

        #region Constructor

        public HapNavigationController(IUsersManager usersManager, IHotelsManager hotelsManager, ISecurityManager securityManager)
        {
            _userManager = usersManager;
            _hotelManager = hotelsManager;
            _securityManager = securityManager;
        }

        #endregion

        #region WebActions

        [HttpGet]
        [ChildActionOnly]
        public ActionResult Menu(string categoty = null)
        {
            var userId = _securityManager.GetCurrentUserId();
            var model = new HotelAdministratorPanelModel()
            {
                UserRoles = _userManager.GetUserRoles(userId),
                CurrentHotels = ConvertToSelectListItem()
            };

            return PartialView("_HotelsMenu", model);
        }

        #endregion

        #region Helper

        private List<SelectListItem> ConvertToSelectListItem()
        {
            var userId = _securityManager.GetCurrentUserId();
            var hotel = _hotelManager.GetHotelsByAdminId(userId);
            return hotel.Select(h => new SelectListItem() { Text = h.Name, Value = h.Id.ToString() }).ToList();
        }

        #endregion
    }
}