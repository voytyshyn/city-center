﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Code;
using CityCenter.WebUI.Frontend.Models.Excursion;
using CityCenter.WebUI.Frontend.Models.Helper;
using CityCenter.WebUI.Frontend.Models.Panel;
using PagedList;

namespace CityCenter.WebUI.Frontend.Controllers
{
    [AuthorizeOrThrow404("ExcursionAdmin")]
    public class ExcursionAdministratorPanelController : Controller
    {
        #region Fields

        private readonly IExcursionsManager _excursionsManager;

        private readonly ISecurityManager _securityManager;

        private readonly IUsersManager _usersManager;

        private readonly IEmailManager _emailManager;

        #endregion

        #region Constructor

        public ExcursionAdministratorPanelController(IExcursionsManager excursionsManager, ISecurityManager securityManager, IUsersManager usersManager, IEmailManager emailManager)
        {
            _excursionsManager = excursionsManager;
            _securityManager = securityManager;
            _usersManager = usersManager;
            _emailManager = emailManager;
        }

        #endregion

        #region WebActions

        [HttpGet]
        public ActionResult Panel()
        {
            return View();
        }

        [HttpGet]
        public ActionResult EditExcursionContent(int excursionId)
        {
            #region Validation

            var userId = _securityManager.GetCurrentUserId();

            if (!_securityManager.IsUserAdministratorOfExcursion(userId, excursionId))
            {
                throw new HttpException(404, "HTTP/1.1 404 Not Found");
            }

            #endregion

            var excursionDiscriptionModel = new ExcursionDescriptionModel()
            {
                Description = _excursionsManager.GetExcursionDescriptionById(excursionId),
                Excursion = _excursionsManager.GetExcursionById(excursionId),
                Name = _excursionsManager.GetExcursionById(excursionId).Name
            };

            var totalSize = _excursionsManager.GetPendingReservationsCountByExcursionId(excursionId);
            ViewBag.CountOfPendingReservation = totalSize;
            return PartialView("_EditExcursionContent", excursionDiscriptionModel);
        }

        [HttpPost]
        public ActionResult EditExcursionContent(ExcursionDescriptionModel model)
        {
            if (ModelState.IsValid)
            {
                _excursionsManager.UpdateExcursionDescription(model.Description);
                _excursionsManager.UpdateExcursionName(model.Excursion.Name, model.Description.ExcursionId);
                return RedirectToAction("Panel");
            }
            return PartialView("_EditExcursionContent");
        }

        [HttpPost]
        public ActionResult UploadNewImage(int excursionId, HttpPostedFileBase image)
        {
            if (image != null)
            {
                var excursionDescription = _excursionsManager.GetExcursionDescriptionById(excursionId);
                excursionDescription.Photo = new byte[image.ContentLength];
                excursionDescription.PhotoExtension = image.ContentType;
                image.InputStream.Read(excursionDescription.Photo, 0, image.ContentLength);
                _excursionsManager.UpdateExcursionImage(excursionDescription);
            }
            return RedirectToAction("EditExcursionContent", new { excursionId });
        }

        [HttpGet]
        public ActionResult PendingReservations(int excursionId, int page = 1)
        {
            #region Validation

            var userId = _securityManager.GetCurrentUserId();

            if (!_securityManager.IsUserAdministratorOfExcursion(userId, excursionId))
            {
                throw new HttpException(404, "HTTP/1.1 404 Not Found");
            }

            #endregion

            var pagedListModel = new PagedListModel()
            {
                Page = page,
                PageSize = 2,
                TotalItems = _excursionsManager.GetPendingReservationsCountByExcursionId(excursionId)
            };

            ViewBag.TotalItems = pagedListModel.TotalItems;
            ViewBag.PageSize = pagedListModel.PageSize;

            #region Paging Validation

            if (page < 1)
            {
                return RedirectToAction("PendingReservations");
            }
            if (page != 1 && (page - 1) * pagedListModel.PageSize >= pagedListModel.TotalItems)
            {
                return RedirectToAction("PendingReservations", new
                {
                    page = Math.Ceiling((double)pagedListModel.TotalItems / pagedListModel.PageSize)
                });
            }

            #endregion

            var reservationEntities = _excursionsManager.GetPendingReservationsByExcursionId(excursionId, pagedListModel.Page,
                pagedListModel.PageSize);
            var reservations = reservationEntities.Select(item => new ExcursionReservationDetailsModel()
            {
                ReservationDetails = item,
                User = _usersManager.GetUserById(item.UserId)
            }).ToList();
            var pagedList = new StaticPagedList<ExcursionReservationDetailsModel>(reservations, page, pagedListModel.PageSize,
                pagedListModel.TotalItems);
            var model = new AdminExcursionReservationDetailsModel()
            {
                ReservationDetails = pagedList,
                ExcursionId = excursionId
            };
            ViewBag.CountOfPendingReservation = _excursionsManager.GetPendingReservationsCountByExcursionId(excursionId);

            return View(model);

        }

        [HttpGet]
        public ActionResult AcceptedReservations(int excursionId, int page = 1)
        {
            #region Validation

            var userId = _securityManager.GetCurrentUserId();

            if (!_securityManager.IsUserAdministratorOfExcursion(userId, excursionId))
            {
                throw new HttpException(404, "HTTP/1.1 404 Not Found");
            }

            #endregion

            var pagedListModel = new PagedListModel()
            {
                Page = page,
                PageSize = 3,
                TotalItems = _excursionsManager.GetAcceptedReservationsCountByExcursionId(excursionId)
            };

            ViewBag.TotalItems = pagedListModel.TotalItems;
            ViewBag.PageSize = pagedListModel.PageSize;

            #region Paging Validation

            if (page < 1)
            {
                return RedirectToAction("AcceptedReservations");
            }
            if (page != 1 && (page - 1) * pagedListModel.PageSize >= pagedListModel.TotalItems)
            {
                return RedirectToAction("AcceptedReservations", new { page = Math.Ceiling((double)pagedListModel.TotalItems / pagedListModel.PageSize) });
            }

            #endregion

            var reservationEntities = _excursionsManager.GetAcceptedReservationsByExcursionId(excursionId, pagedListModel.Page,
               pagedListModel.PageSize);
            var reservations = reservationEntities.Select(item => new ExcursionReservationDetailsModel()
            {
                ReservationDetails = item,
                User = _usersManager.GetUserById(item.UserId)
            }).ToList();
            var pagedList = new StaticPagedList<ExcursionReservationDetailsModel>(reservations, page, pagedListModel.PageSize,
                  pagedListModel.TotalItems);
            var model = new AdminExcursionReservationDetailsModel()
            {
                ReservationDetails = pagedList,
                ExcursionId = excursionId
            };
            ViewBag.CountOfPendingReservation = _excursionsManager.GetPendingReservationsCountByExcursionId(excursionId);

            return View(model);
        }

        [HttpPost]
        public ActionResult AcceptReservation(int reservationId, int excursionId)
        {
            var hotelName = _excursionsManager.GetExcursionById(excursionId).Name;
            _excursionsManager.AcceptReservation(reservationId);
            SendEmailReservationNotification(hotelName, reservationId, reservationStatus: "Accepted");
            return RedirectToAction("PendingReservations", new { excursionId });
        }

        [HttpPost]
        public ActionResult DeclineReservation(int reservationId, int excursionId)
        {
            var hotelName = _excursionsManager.GetExcursionById(excursionId).Name;
            _excursionsManager.DeclineReservation(reservationId);
            SendEmailReservationNotification(hotelName, reservationId, reservationStatus: "Declined");
            return RedirectToAction("PendingReservations", new { excursionId });
        }

        #endregion

        #region Helper

        public FileContentResult GetHotelImage(int excursionId)
        {
            var desc = _excursionsManager.GetExcursionDescriptionById(excursionId);
            return File(desc.Photo, MimeMapping.GetMimeMapping(desc.PhotoExtension));
        }

        private void SendEmailReservationNotification(string excursionName, int reservationId, string reservationStatus)
        {
            var userId = _usersManager.GetUserIdByReservationId(reservationId);
            var user = _usersManager.GetUserById(userId);

            var mailSubject = "Reservation Notification";
            var mailBody = string.Format(
                "<div style=\"font-size:16px;\">Dear {0} {1}.<br/>"
                + "Your Registration to excursion {2} was : {3}",
                user.FirstName, user.Surname, excursionName, reservationStatus);

            _emailManager.SendEmail(user.Email, mailSubject, mailBody);
        }

        #endregion
    }
}