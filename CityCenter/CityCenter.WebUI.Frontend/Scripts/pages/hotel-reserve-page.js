(function ($) {
    var HotelReserve = function ($rootElement) {
        var _self = this;

        var _private = {};

        var txtEmpty = "";

        _private.$rootElement = $rootElement;

        _private.initialize = function () {
            _private.$beginDate = $rootElement.find("#BeginDate");
            _private.$endDate = $rootElement.find("#EndDate");
            _private.$roomType = $rootElement.find("#room-type");
            _private.$wrapperInput = $rootElement.find("#wrapper-input");
            _private.$checkbox = $rootElement.find(".checkbox");            
            _private.$hotelIdHidden = $rootElement.find("input[type='hidden']");
            _private.$beginDate.on("change", _private.checkDate_Blur);
            _private.$endDate.on("change", _private.checkDate_Blur);

            _private.hotelId = _private.$hotelIdHidden.val();

            _private.beginDate();
            _private.endDate();
            _private.checkboxUncheck();
        };

        _private.checkDate_Blur = function () {
            if ($(_private.$beginDate).val() != txtEmpty & $(_private.$endDate).val() != txtEmpty) {

                $.ajax({
                    url: "/HotelBooking/GetAvailableRoomTypes",
                    type: "POST",
                    dataType: "json",
                    data: {
                        hotelId: _private.hotelId,
                        beginDate: _private.$beginDate.val(),
                        endDate: _private.$endDate.val()
                    }
                }).done(function (data) {

                    _private.$roomType.find("option").each(function () {
                        var $option = $(this);
                        var optionId = $option.val();
                        if (optionId) {
                            if ($.inArray(parseInt(optionId), data.availableRoomTypeIds) >= 0) {
                                $option.show();
                            } else {
                                $option.hide();
                                $option.removeAttr("selected");
                            }
                        }
                    });                    

                    $(_private.$roomType).removeClass("hide");
                    $(_private.$roomType).addClass("show");
                    $(_private.$wrapperInput).height("400px");
                }).fail(function () {

                });                
            }
        };

        _private.beginDate = function () {
            $(_private.$beginDate).datepicker({
                minDate: new Date(),
                changeYear: true,
                changeMonth: true,
                yearRange: "-0:+1",
                dateFormat: "DD, d MM, yy",
                maxDate: "+1Y",
                onClose: function (selectedDate) {
                    $(_private.$endDate).datepicker("option", "minDate", selectedDate);
                }
            });
        };

        _private.endDate = function () {
            $(_private.$endDate).datepicker({
                minDate: new Date(),
                changeYear: true,
                changeMonth: true,
                yearRange: "-0:+1",
                maxDate: "+1Y",
                dateFormat: "DD, d MM, yy",
                onClose: function (selectedDate) {
                    $(_private.$beginDate).datepicker("option", "maxDate", selectedDate);
                }
            });
        };

        _private.checkboxUncheck = function () {
            $(_private.$checkbox).prop("checked", false);
        };

        _private.initialize();

    };

    $(document).on("ready", function () {
        var page = new HotelReserve($("#request-form"));
    });

})(window.jQuery);