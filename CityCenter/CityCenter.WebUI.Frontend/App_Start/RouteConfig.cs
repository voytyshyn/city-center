﻿using System.Web.Mvc;
using System.Web.Routing;

namespace CityCenter.WebUI.Frontend
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: null,
                url: "UserPanel",
                defaults: new { controller = "UserPanel", action = "Panel" }
            );

            routes.MapRoute(
               name: null,
               url: "UserPanel/MyBookings",
               defaults: new { controller = "UserPanel", action = "Reservations" }
           );

            routes.MapRoute(
                name: null,
                url: "AdminPanel/Excursions",
                defaults: new { controller = "ExcursionAdministratorPanel", action = "Panel" }
            );

            routes.MapRoute(
                name: null,
                url: "AdminPanel/MyExcursions/{excursionId}/Edit",
                defaults: new { controller = "ExcursionAdministratorPanel", action = "EditExcursionContent" },
                constraints: new { excursionId = @"\d+" }
            );

            routes.MapRoute(
                 name: null,
                 url: "AdminPanel/MyExcursions/{excursionId}/{action}",
                 defaults: new { controller = "ExcursionAdministratorPanel" },
                 constraints: new { excursionId = @"\d+" }
             );

            routes.MapRoute(
                name: null,
                url: "AdminPanel/Hotels",
                defaults: new { controller = "HotelAdministratorPanel", action = "Panel" }
            );

            routes.MapRoute(
                name: null,
                url: "AdminPanel/MyHotels/{hotelId}/Edit",
                defaults: new { controller = "HotelAdministratorPanel", action = "EditHotelContent" },
                constraints: new { hotelId = @"\d+" }
            );

            routes.MapRoute(
                 name: null,
                 url: "AdminPanel/MyHotels/{hotelId}/{action}",
                 defaults: new { controller = "HotelAdministratorPanel" },
                 constraints: new { hotelId = @"\d+" }
             );

            routes.MapRoute(
                name: null,
                url: "AddNew/Excursion",
                defaults: new { controller = "AddNew", action = "AddNewExcursion" }
            );

            routes.MapRoute(
                name: null,
                url: "AddNew/Hotel",
                defaults: new { controller = "AddNew", action = "AddNewHotel" }
            );

            routes.MapRoute(
                name: null,
                url: "Hotels/{hotelId}/BookRoom",
                defaults: new { controller = "HotelBooking", action = "HotelBooking" },
                constraints: new { hotelId = @"\d+" }
            );

            routes.MapRoute(
                name: null,
                url: "Excursions/{excursionId}/Register",
                defaults: new { controller = "ExcursionRegistration", action = "ExcursionRegistration" },
                constraints: new { excursionId = @"\d+" }
            );

            routes.MapRoute(
                name: null,
                url: "Hotels",
                defaults: new { controller = "Hotels", action = "Hotels"}
            );

            routes.MapRoute(
                name: null,
                url: "Excursions",
                defaults: new { controller = "Excursions", action = "Excursions" }
            );

            routes.MapRoute(
                name: "Error404",
                url: "Error404/{action}/{id}",
                defaults: new { controller = "Error404", action = "Error404", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                 defaults: new { controller = "MainPage", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}