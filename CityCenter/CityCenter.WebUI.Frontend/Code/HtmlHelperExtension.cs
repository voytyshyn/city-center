﻿using System.Web.Mvc;

namespace CityCenter.WebUI.Frontend.Code
{
    public static class HtmlHelperExtension
    {
        public static string ActivePage(this HtmlHelper helper, string action, string controller)
        {
            var classValue = "";

            var currentController = helper.ViewContext.Controller.ValueProvider.GetValue("controller").RawValue.ToString();
            var currentAction = helper.ViewContext.Controller.ValueProvider.GetValue("action").RawValue.ToString();

            if (currentController == controller && currentAction == action)
            {
                classValue = "selected";
            }

            return classValue;
        }
    }
}