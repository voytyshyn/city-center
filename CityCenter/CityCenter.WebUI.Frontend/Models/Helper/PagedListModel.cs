﻿namespace CityCenter.WebUI.Frontend.Models.Helper
{
    public class PagedListModel
    {
        public int Page { get; set; }

        public int PageSize { get; set; }

        public int TotalItems { get; set; }
    }
}