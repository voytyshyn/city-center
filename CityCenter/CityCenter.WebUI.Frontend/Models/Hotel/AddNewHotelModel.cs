﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CityCenter.WebUI.Frontend.Models.Hotel
{
    public class AddNewHotelModel
    {
        [Required]
        [StringLength(20, ErrorMessage = "Length should not exceed 20")]
        [RegularExpression(@"^[a-zA-Z,. ()-]{1,20}", ErrorMessage = "Invalid hotel name")]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [RegularExpression(@"^[1-5]{1}", ErrorMessage = "Invalid rating")]
        public int Rating { get; set; }

        [Required]
        public bool Parking { get; set; }

        [Required]
        public bool Pool { get; set; }

        [Required]
        public bool Sauna { get; set; }

        [Required]
        public bool TennisCourt { get; set; }

        [Required]
        public bool Gym { get; set; }

        [Required]
        public string Header { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [RegularExpression(@"^[0-9,. ()-]{1,20}", ErrorMessage = "Invalid phone")]
        public string Phone { get; set; }

        [Required]
        public string Site { get; set; }

        [Required]
        [DisplayName("Room Type")]
        [RegularExpression(@"^[a-zA-Z,. ()-]{1,20}", ErrorMessage = "Invalid type name")]
        public string RoomType1 { get; set; }

        [Required]
        [DisplayName("Price")]
        public decimal? Price1 { get; set; }

        [Required]
        [DisplayName("Rooms Count")]
        public int? RoomsCount1 { get; set; }
        
        public string RoomType2 { get; set; }

        public decimal? Price2 { get; set; }

        public int? RoomsCount2 { get; set; }
        
        public string RoomType3 { get; set; }

        public decimal? Price3 { get; set; }

        public int? RoomsCount3 { get; set; }
        
        public string RoomType4 { get; set; }

        public decimal? Price4 { get; set; }

        public int? RoomsCount4 { get; set; }

    }
}