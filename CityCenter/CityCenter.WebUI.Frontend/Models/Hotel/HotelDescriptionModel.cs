﻿using CityCenter.Entities;

namespace CityCenter.WebUI.Frontend.Models.Hotel
{
    public class HotelDescriptionModel
    {
        public string HotelName { get; set; }

        public HotelDescription HotelDescription { get; set; }
    }
}