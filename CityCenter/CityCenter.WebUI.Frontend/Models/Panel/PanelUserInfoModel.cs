﻿using System.ComponentModel.DataAnnotations;

namespace CityCenter.WebUI.Frontend.Models.Panel
{
    public class PanelUserInfoModel
    {
        [Required(ErrorMessage = "Name is required")]
        [StringLength(30, ErrorMessage = "Length should not exceed 30")]
        [RegularExpression(@"^[a-zA-Zа-яА-ЯіІ]{1,30}", ErrorMessage = "Invalid first name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Surname is required")]
        [StringLength(30, ErrorMessage = "Length should not exceed 30")]
        [RegularExpression(@"^[a-zA-Zа-яА-ЯіІ]{1,30}", ErrorMessage = "Invalid surname")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Phone is required")]
        [StringLength(13, ErrorMessage = "Length should be 13")]
        [RegularExpression(@"^\+[1-9]{1}[0-9]{11}$", ErrorMessage = "Invalid Phone")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [StringLength(50, ErrorMessage = "Length should not exceed 50")]
        [RegularExpression(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", ErrorMessage = "Invalid email")]
        public string Email { get; set; }
    }
}