﻿using PagedList;

namespace CityCenter.WebUI.Frontend.Models.Panel
{
    public class AdminExcursionReservationDetailsModel
    {
        public StaticPagedList<ExcursionReservationDetailsModel> ReservationDetails { get; set; }

        public int ExcursionId { get; set; }
    }
}