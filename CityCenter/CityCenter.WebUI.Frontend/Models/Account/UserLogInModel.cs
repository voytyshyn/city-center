﻿using System.ComponentModel.DataAnnotations;

namespace CityCenter.WebUI.Frontend.Models.Account
{
    public class UserLogInModel
    {
        [Required]
        [StringLength(20, ErrorMessage = "Length should not exceed 20")]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
    }
}