﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CityCenter.WebUI.Frontend.Models.Account
{
    public class СhangePasswordModel
    {
        [Required]
        [Remote("IsOldPasswordValid", "UserPanel", ErrorMessage = "Incorrect old password")]
        public string OldPassword { get; set; }

        [Required]
        [Remote("IsNewPasswordValid", "UserPanel", ErrorMessage = "New password and old password are equal", AdditionalFields = "OldPassword")]
        [RegularExpression(@"^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,30}$", ErrorMessage = "Password must have large and small letters, numbers. Min-Length: 8 symbol. Max-Length: 30 symbol.")]
        [StringLength(30, ErrorMessage = "Length should not exceed 30")]
        public string NewPassword { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Length should not exceed 30")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword")]
        public string NewPasswordConfirmation { get; set; }
    }
}