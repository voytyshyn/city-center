﻿using System;

namespace CityCenter.Entities
{
    public class HotelReservationDetails
    {
        public int Id { get; set; }

        public int HotelId { get; set; }

        public string HotelName { get; set; }

        public string RoomType { get; set; }

        public decimal Price { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool Parking { get; set; }

        public bool Pool { get; set; }

        public bool Sauna { get; set; }

        public bool TennisCourt { get; set; }

        public bool Gym { get; set; }

        public int Status { get; set; }
    }
}
