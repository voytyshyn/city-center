﻿using System;

namespace CityCenter.Entities
{
    public class ExcursionReservationDetails
    {
        public int ReservationId { get; set; }

        public int UserId { get; set; }

        public int ExcursionId { get; set; }

        public string ExcursionName { get; set; }

        public decimal Price { get; set; }

        public DateTime Date { get; set; }

        public TimeSpan BeginTime { get; set; }

        public TimeSpan Duration { get; set; }

        public int Status { get; set; }
    }
}
