﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using CityCenter.Entities;
using CityCenter.Repositories.Abstract;

namespace CityCenter.Repositories.Sql
{
    public class ExcursionRepository : IExcursionsRepository
    {
        #region Queries

        private const string INSERT_RESERVATION = @"INSERT INTO tblExcursionReservation ([UserId], [ExcursionId], [ExcursionDate], [Status])
                                                   VALUES (@UserId, @ExcursionId, @ExcursionDate, @Status) SELECT CAST(SCOPE_IDENTITY() AS INT);";

        private const string GET_EXCURSIONS = @"SELECT [Id], [Name], [Address], [Duration], [BeginTime], [Price]
                                                FROM tblExcursion";

        private const string GET_DESCRIPTION = @"SELECT [ExcursionId], [ShortDescription], [LongDescription], [Photo], [PhotoExtension]
                                                       FROM tblExcursionDescription";

        private const string GET_EXCURSIONS_PAGING = @"SELECT [Id], [Name], [Address], [Duration], [BeginTime], [Price]
                                                       FROM (SELECT ROW_NUMBER() OVER(ORDER BY [Id]) AS NUMBER, * FROM tblExcursion ) AS TBL
                                                       WHERE NUMBER BETWEEN ((@PageNumber - 1) * @PageSize + 1) AND (@PageNumber * @PageSize)";

        private const string GET_DESCRIPTION_PAGING = @"SELECT [ExcursionId], [ShortDescription], [LongDescription], [Photo], [PhotoExtension]
                                                        FROM (SELECT ROW_NUMBER() OVER(ORDER BY [ExcursionId]) AS NUMBER, * FROM tblExcursionDescription ) AS TBL
                                                        WHERE NUMBER BETWEEN ((@PageNumber - 1) * @PageSize + 1) AND (@PageNumber * @PageSize)
                                                        ORDER BY [ExcursionId]";

        private const string GET_EXCURSION_BY_ID = @"SELECT [Id], [Name], [Address], [Duration], [BeginTime], [Price]
                                                     FROM tblExcursion WHERE Id=@Id";

        private const string GET_EXCURSION_BY_NAME = @"SELECT [Id], [Name], [Address], [Duration], [BeginTime], [Price]
                                                       FROM tblExcursion WHERE Name=@Name";

        private const string GET_DESCRIPTIONS_COUNT = @"SELECT COUNT(*) FROM tblExcursionDescription";

        private const string GET_DESCRIPTIONS_BY_ID = @"SELECT [ExcursionId], [ShortDescription], [LongDescription], [Photo], [PhotoExtension]
                                                    FROM tblExcursionDescription WHERE ExcursionId=@ExcursionId";

        private const string GET_RESERVATION_DETAILS_COUNT_BY_USERID = @"SELECT COUNT(*)
                                                                         FROM tblExcursion exc
                                                                         INNER JOIN tblExcursionReservation res
                                                                         ON res.ExcursionId = exc.Id
                                                                         WHERE res.UserId=@UserId";

        private const string GET_RESERVATION_DETAILS_BY_USERID_PAGING = @"SELECT Id, UserId, ExcursionId, ExcursionDate, [Status], [Name], [Duration], [Price], [BeginTime]
                                                                          FROM (SELECT ROW_NUMBER() OVER(ORDER BY res.ExcursionDate) AS NUMBER, res.[Id], res.[UserId], res.[ExcursionId], res.[ExcursionDate], res.[Status], exc.[Name], exc.[Duration], exc.[Price], exc.[BeginTime] 
                                                                          FROM tblExcursion exc
                                                                          INNER JOIN tblExcursionReservation res
                                                                          ON res.ExcursionId = exc.Id 
                                                                          WHERE res.UserId = @UserId ) AS TBL
                                                                          WHERE NUMBER BETWEEN ((@PageNumber - 1) * @PageSize + 1) AND (@PageNumber * @PageSize)";

        private const string GET_ACCEPTED_RESERVATIONS_COUNT_BY_EXCURSIONID = @"SELECT COUNT(*)
                                                                         FROM tblExcursion exc
                                                                         INNER JOIN tblExcursionReservation res
                                                                         ON res.ExcursionId = exc.Id
                                                                         WHERE res.ExcursionId = @ExcursionId AND res.Status != 0";

        private const string GET_ACCEPTED_RESERVATIONS_BY_EXCURSIONID_PAGING = @"SELECT Id, UserId, ExcursionId, ExcursionDate, [Status], [Name], [Duration], [Price], [BeginTime]
                                                                          FROM (SELECT ROW_NUMBER() OVER(ORDER BY res.ExcursionDate) AS NUMBER, res.[Id], res.[UserId], res.[ExcursionId], res.[ExcursionDate], res.[Status], exc.[Name], exc.[Duration], exc.[Price], exc.[BeginTime] 
                                                                          FROM tblExcursion exc
                                                                          INNER JOIN tblExcursionReservation res
                                                                          ON res.ExcursionId = exc.Id 
                                                                          WHERE res.ExcursionId = @ExcursionId AND res.Status != 0) AS TBL
                                                                          WHERE NUMBER BETWEEN ((@PageNumber - 1) * @PageSize + 1) AND (@PageNumber * @PageSize)";

        private const string GET_PENDING_RESERVATIONS_COUNT_BY_EXCURSIONID = @"SELECT COUNT(*)
                                                                         FROM tblExcursion exc
                                                                         INNER JOIN tblExcursionReservation res
                                                                         ON res.ExcursionId = exc.Id
                                                                         WHERE res.ExcursionId = @ExcursionId AND res.Status = 0";

        private const string GET_PENDING_RESERVATIONS_BY_EXCURSIONID_PAGING = @"SELECT Id, UserId, ExcursionId, ExcursionDate, [Status], [Name], [Duration], [Price], [BeginTime]
                                                                          FROM (SELECT ROW_NUMBER() OVER(ORDER BY res.ExcursionDate) AS NUMBER, res.[Id], res.[UserId], res.[ExcursionId], res.[ExcursionDate], res.[Status], exc.[Name], exc.[Duration], exc.[Price], exc.[BeginTime] 
                                                                          FROM tblExcursion exc
                                                                          INNER JOIN tblExcursionReservation res
                                                                          ON res.ExcursionId = exc.Id 
                                                                          WHERE res.ExcursionId = @ExcursionId AND res.Status = 0) AS TBL
                                                                          WHERE NUMBER BETWEEN ((@PageNumber - 1) * @PageSize + 1) AND (@PageNumber * @PageSize)";

        private const string UPDATE_EXCURSION_DESCRIPTION = @"UPDATE tblExcursionDescription
                                                              SET LongDescription=@LongDescription, ShortDescription=@ShortDescription
                                                              WHERE ExcursionId=@ExcursionId;";

        private const string UPDATE_EXCURSION_NAME = @"UPDATE tblExcursion
                                                       SET Name=@Name
                                                       WHERE Id=@ExcursionId;";

        private const string UPDATE_EXCURSION_PHOTO = @"UPDATE tblExcursionDescription
                                                        SET Photo=@Photo, PhotoExtension=@PhotoExtension
                                                        WHERE ExcursionId=@ExcursionId;";

        private const string CHANGE_RESERVATION_STATUS = @"UPDATE tblExcursionReservation
                                                           SET Status=@Status
                                                           WHERE Id=@ReservationId;";

        private const string GET_EXCURSIONS_BY_ADMIN_ID = @"SELECT e.* FROM tblExcursion e 
                                                       INNER JOIN tblExcursionAdministrators a
                                                       ON e.id = a.ExcursionId
                                                       WHERE a.UserId = @AdminId";

        private const string GET_TODAY_PENDING_RESERVATIONS = @"SELECT res.[Id], res.[UserId], res.[ExcursionId], res.[ExcursionDate], res.[Status], exc.[Name], exc.[Duration], exc.[Price], exc.[BeginTime] 
                                                                FROM tblExcursion exc
                                                                INNER JOIN tblExcursionReservation res
                                                                ON res.ExcursionId = exc.Id 
                                                                WHERE res.ExcursionDate <= GETDATE() AND res.Status = 0";


        private const string INSERT_NEW_EXCURSION = @"INSERT INTO tblExcursion ([Name], [Address], [Duration], [BeginTime], [Price])
                                                      VALUES (@Name, @Address, @Duration, @BeginTime, @Price);SELECT CAST(SCOPE_IDENTITY() AS INT);";

        private const string INSERT_NEW_EXCURSION_DESCRIPTION = @"INSERT INTO tblExcursionDescription ([ExcursionId], [ShortDescription], [LongDescription], [Photo], [PhotoExtension])
                                                                  VALUES (@ExcursionId, @ShortDescription, @LongDescription, @Photo, @PhotoExtension);SELECT CAST(SCOPE_IDENTITY() AS INT);";

        private const string INSERT_NEW_EXCURSION_ADMIN = @"INSERT INTO tblExcursionAdministrators ([UserId], [ExcursionId])
                                                            VALUES (@UserId, @ExcursionId)";


        #endregion

        #region Fields

        private readonly string _connectionString;

        #endregion

        #region Constructors

        public ExcursionRepository(string connStr)
        {
            _connectionString = connStr;
        }

        #endregion

        #region IExcursionsRepository

        public int InsertReservation(int userId, int excursionId, DateTime excursionDate, int status)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(INSERT_RESERVATION, conn))
                {
                    cmd.Parameters.AddWithValue("UserId", userId);
                    cmd.Parameters.AddWithValue("ExcursionId", excursionId);
                    cmd.Parameters.AddWithValue("ExcursionDate", excursionDate);
                    cmd.Parameters.AddWithValue("Status", status);

                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public List<Excursion> GetExcursions()
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_EXCURSIONS, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var excursions = new List<Excursion>();
                        while (reader.Read())
                        {
                            excursions.Add(ReadExcursion(reader));
                        }
                        return excursions.Count != 0 ? excursions : null;
                    }
                }
            }
        }

        public List<Excursion> GetExcursions(int pageNumber, int pageSize)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_EXCURSIONS_PAGING, conn))
                {
                    cmd.Parameters.AddWithValue("@PageSize", pageSize);
                    cmd.Parameters.AddWithValue("@PageNumber", pageNumber);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var excursions = new List<Excursion>();
                        while (reader.Read())
                        {
                            excursions.Add(ReadExcursion(reader));
                        }
                        return excursions.Count != 0 ? excursions : null;
                    }
                }
            }
        }

        public List<ExcursionDescription> GetExcursionDescriptions()
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_DESCRIPTION, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var excursions = new List<ExcursionDescription>();
                        while (reader.Read())
                        {
                            excursions.Add(ReadExcursionDescription(reader));
                        }
                        return excursions.Count != 0 ? excursions : null;
                    }
                }
            }
        }

        public List<ExcursionDescription> GetExcursionDescriptions(int pageNumber, int pageSize)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_DESCRIPTION_PAGING, conn))
                {
                    cmd.Parameters.AddWithValue("@PageSize", pageSize);
                    cmd.Parameters.AddWithValue("@PageNumber", pageNumber);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var excursions = new List<ExcursionDescription>();
                        while (reader.Read())
                        {
                            excursions.Add(ReadExcursionDescription(reader));
                        }
                        return excursions.Count != 0 ? excursions : null;
                    }
                }
            }
        }

        public int GetExcursionDescriptionsCount()
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_DESCRIPTIONS_COUNT, conn))
                {
                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public Excursion GetExcursionById(int id)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_EXCURSION_BY_ID, conn))
                {
                    cmd.Parameters.AddWithValue("@Id", id);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var excursion = new Excursion();
                        while (reader.Read())
                        {
                            excursion = ReadExcursion(reader);
                        }

                        return (excursion.Name == null) ? null : excursion;
                    }
                }
            }
        }

        public Excursion GetExcursionByName(string name)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_EXCURSION_BY_NAME, conn))
                {
                    cmd.Parameters.AddWithValue("@Name", name);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var excursion = new Excursion();
                        while (reader.Read())
                        {
                            excursion = ReadExcursion(reader);
                        }

                        return (excursion.Name == null) ? null : excursion;
                    }
                }
            }
        }

        public ExcursionDescription GetExcursionDescriptionById(int excursionId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_DESCRIPTIONS_BY_ID, conn))
                {
                    cmd.Parameters.AddWithValue("@ExcursionId", excursionId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var excursionDescription = new ExcursionDescription();
                        while (reader.Read())
                        {
                            excursionDescription = ReadExcursionDescription(reader);
                        }

                        return (excursionDescription.ShortDescription == null) ? null : excursionDescription;
                    }
                }
            }
        }

        public int GetReservationDetailsCountByUserId(int userId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_RESERVATION_DETAILS_COUNT_BY_USERID, conn))
                {
                    cmd.Parameters.AddWithValue("@UserId", userId);

                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public List<ExcursionReservationDetails> GetReservationDetailsByUserId(int userId, int pageNumber, int pageSize)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_RESERVATION_DETAILS_BY_USERID_PAGING, conn))
                {
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    cmd.Parameters.AddWithValue("@PageSize", pageSize);
                    cmd.Parameters.AddWithValue("@PageNumber", pageNumber);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var reservations = new List<ExcursionReservationDetails>();
                        while (reader.Read())
                        {
                            reservations.Add(ReadExcursionReservationDetails(reader));
                        }
                        return reservations;
                    }
                }
            }
        }

        public int GetAcceptedReservationsCountByExcursionId(int excursionId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_ACCEPTED_RESERVATIONS_COUNT_BY_EXCURSIONID, conn))
                {
                    cmd.Parameters.AddWithValue("@ExcursionId", excursionId);

                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public List<ExcursionReservationDetails> GetAcceptedReservationsByExcursionId(int excursionId, int pageNumber, int pageSize)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_ACCEPTED_RESERVATIONS_BY_EXCURSIONID_PAGING, conn))
                {
                    cmd.Parameters.AddWithValue("@ExcursionId", excursionId);
                    cmd.Parameters.AddWithValue("@PageSize", pageSize);
                    cmd.Parameters.AddWithValue("@PageNumber", pageNumber);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var reservations = new List<ExcursionReservationDetails>();
                        while (reader.Read())
                        {
                            reservations.Add(ReadExcursionReservationDetails(reader));
                        }
                        return reservations;
                    }
                }
            }
        }

        public int GetPendingReservationsCountByExcursionId(int excursionId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_PENDING_RESERVATIONS_COUNT_BY_EXCURSIONID, conn))
                {
                    cmd.Parameters.AddWithValue("@ExcursionId", excursionId);

                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public List<ExcursionReservationDetails> GetPendingReservationsByExcursionId(int excursionId, int pageNumber, int pageSize)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_PENDING_RESERVATIONS_BY_EXCURSIONID_PAGING, conn))
                {
                    cmd.Parameters.AddWithValue("@ExcursionId", excursionId);
                    cmd.Parameters.AddWithValue("@PageSize", pageSize);
                    cmd.Parameters.AddWithValue("@PageNumber", pageNumber);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var reservations = new List<ExcursionReservationDetails>();
                        while (reader.Read())
                        {
                            reservations.Add(ReadExcursionReservationDetails(reader));
                        }
                        return reservations;
                    }
                }
            }
        }

        public void UpdateExcursionImage(ExcursionDescription excursionDescription)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(UPDATE_EXCURSION_PHOTO, conn))
                {
                    cmd.Parameters.AddWithValue("Photo", excursionDescription.Photo);
                    cmd.Parameters.AddWithValue("PhotoExtension", excursionDescription.PhotoExtension);
                    cmd.Parameters.AddWithValue("ExcursionId", excursionDescription.ExcursionId);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void UpdateExcursionDescription(ExcursionDescription excursionDescription)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(UPDATE_EXCURSION_DESCRIPTION, conn))
                {
                    cmd.Parameters.AddWithValue("ShortDescription", excursionDescription.ShortDescription);
                    cmd.Parameters.AddWithValue("LongDescription", excursionDescription.LongDescription);
                    cmd.Parameters.AddWithValue("ExcursionId", excursionDescription.ExcursionId);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void UpdateExcursionName(string name, int id)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(UPDATE_EXCURSION_NAME, conn))
                {
                    cmd.Parameters.AddWithValue("Name", name);
                    cmd.Parameters.AddWithValue("ExcursionId", id);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void ChangeReservationStatus(int reservationId, int status)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(CHANGE_RESERVATION_STATUS, conn))
                {
                    cmd.Parameters.AddWithValue("ReservationId", reservationId);
                    cmd.Parameters.AddWithValue("Status", status);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<Excursion> GetExcursionsByAdminId(int adminId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_EXCURSIONS_BY_ADMIN_ID, conn))
                {
                    cmd.Parameters.AddWithValue("AdminId", adminId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var excursion = new List<Excursion>();
                        while (reader.Read())
                        {
                            excursion.Add(ReadExcursion(reader));
                        }
                        return excursion;
                    }
                }
            }
        }

        public int InsertExcursion(Excursion excursion)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(INSERT_NEW_EXCURSION, conn))
                {
                    cmd.Parameters.AddWithValue("Name", excursion.Name);
                    cmd.Parameters.AddWithValue("Address", excursion.Address);
                    cmd.Parameters.AddWithValue("Duration", excursion.Duration);
                    cmd.Parameters.AddWithValue("BeginTime", excursion.BeginTime);
                    cmd.Parameters.AddWithValue("Price", excursion.Price);

                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public void InsertExcursionDescription(ExcursionDescription excursion)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(INSERT_NEW_EXCURSION_DESCRIPTION, conn))
                {
                    cmd.Parameters.AddWithValue("ExcursionId", excursion.ExcursionId);
                    cmd.Parameters.AddWithValue("ShortDescription", excursion.ShortDescription);
                    cmd.Parameters.AddWithValue("LongDescription", excursion.LongDescription);
                    cmd.Parameters.AddWithValue("Photo", excursion.Photo);
                    cmd.Parameters.AddWithValue("PhotoExtension", excursion.PhotoExtension);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void InsertNewExcursionAdmin(int excursionId, int userId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(INSERT_NEW_EXCURSION_ADMIN, conn))
                {
                    cmd.Parameters.AddWithValue("ExcursionId", excursionId);
                    cmd.Parameters.AddWithValue("UserId", userId);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<ExcursionReservationDetails> GetTodayPendingReservations()
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_TODAY_PENDING_RESERVATIONS, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var reservations = new List<ExcursionReservationDetails>();
                        while (reader.Read())
                        {
                            reservations.Add(ReadExcursionReservationDetails(reader));
                        }
                        return reservations;
                    }
                }
            }
        }

        #endregion

        #region Helpers

        private Excursion ReadExcursion(SqlDataReader reader)
        {
            var excursion = new Excursion
            {
                Id = (int)reader["Id"],
                Name = (string)reader["Name"],
                Address = (string)reader["Address"],
                Duration = (TimeSpan)reader["Duration"],
                BeginTime = (TimeSpan)reader["BeginTime"],
                Price = (decimal)reader["Price"]
            };

            return excursion;
        }

        private ExcursionDescription ReadExcursionDescription(SqlDataReader reader)
        {
            var excursion = new ExcursionDescription()
            {
                ExcursionId = (int)reader["ExcursionId"],
                ShortDescription = (string)reader["ShortDescription"],
                LongDescription = (string)reader["LongDescription"],
                Photo = (byte[])reader["Photo"],
                PhotoExtension = (string)reader["PhotoExtension"]
            };

            return excursion;
        }

        private ExcursionReservationDetails ReadExcursionReservationDetails(SqlDataReader reader)
        {
            var excursion = new ExcursionReservationDetails()
            {
                ReservationId = (int)reader["Id"],
                UserId = (int)reader["UserId"],
                ExcursionId = (int)reader["ExcursionId"],
                ExcursionName = (string)reader["Name"],
                Date = (DateTime)reader["ExcursionDate"],
                BeginTime = (TimeSpan)reader["BeginTime"],
                Duration = (TimeSpan)reader["Duration"],
                Status = (int)reader["Status"],
                Price = (decimal)reader["Price"]
            };

            return excursion;
        }

        #endregion
    }
}
