﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using CityCenter.Entities;
using CityCenter.Repositories.Abstract;

namespace CityCenter.Repositories.Sql
{
    public class UserRepository : IUserRepository
    {
        #region Queries

        private const string INSERT_USER_FEEDBACK = @"INSERT INTO tblUserFeedback ([Login], Email, [Message])
                                                  VALUES (@Login, @Email, @Message);";

        private const string INSERT_USER = @"INSERT INTO tblUser ([Login], [Password], Firstname, Surname, Email, Phone, [Status])
                                                  VALUES (@Login, @Password, @Firstname, @Surname, @Email, @Phone, @Status); SELECT CAST(SCOPE_IDENTITY() AS INT);";

        private const string INSERT_UNCONFIRMED_REGISTRATION = @"INSERT INTO tblUnconfirmedRegistration ([UserId], [RegistrationToken], [RegistrationTime])
                                                  VALUES (@UserId, @Token, @Time); SELECT CAST(SCOPE_IDENTITY() AS INT);";

        private const string INSERT_PASSWORD_RECOVERY_REQUEST = @"INSERT INTO tblPasswordRecovery ([UserId], [PasswordRecoveryToken], [PasswordRecoveryTime])
                                                  VALUES (@UserId, @Token, @Time); SELECT CAST(SCOPE_IDENTITY() AS INT);";


        private const string DELETE_USER_TABLE_USER = @"DELETE FROM tblUser WHERE Id=@userId;";

        private const string DELETE_USER_TABLE_USERROLES = @"DELETE FROM tblUserRoles WHERE userId=@userId;";

        private const string UPDATE_USER = @"UPDATE tblUser
                                             SET Login=@Login, Firstname=@Firstname, Surname=@Surname, Email=@Email, Phone=@Phone, Status=@Status
                                             WHERE Id=@UserId;";

        private const string UPDATE_USER_PASSWORD = @"UPDATE tblUser
                                             SET Password=@Password
                                             WHERE Id=@UserId;";

        private const string GET_ALL_USERS = @"SELECT [Id], [Login], [Password], [FirstName], [Surname], [Email], [Phone] ,[Status]
                                               FROM tblUser";

        private const string GET_USER_BY_ID = @"SELECT [Id], [Login], [Password], [FirstName], [Surname], [Email], [Phone] ,[Status]
                                               FROM tblUser WHERE [Id] = @UserId";

        private const string GET_USER_ID_BY_RESERVATION_ID = @"SELECT UserId
                                               FROM tblHotelReservation WHERE Id = @ReservationId";

        private const string GET_USER_BY_LOGIN = @"SELECT [Id], [Login], [Password], [FirstName], [Surname], [Email], [Phone] ,[Status]
                                               FROM tblUser WHERE [Login] = @Login";

        private const string GET_USER_BY_EMAIL = @"SELECT [Id], [Login], [Password], [FirstName], [Surname], [Email], [Phone] ,[Status]
                                               FROM tblUser WHERE [Email] = @Email";

        private const string GET_USER_BY_PHONE = @"SELECT [Id], [Login], [Password], [FirstName], [Surname], [Email], [Phone] ,[Status]
                                               FROM tblUser WHERE [Phone] = @Phone";

        private const string GET_USER_REGISTRATION_TOKEN = @"SELECT TOP 1(RegistrationToken) FROM tblUnconfirmedRegistration WHERE UserID = @UserId";


        private const string GET_USER_PASSWORD_RECOVERY_TOKEN = @"SELECT (PasswordRecoveryToken) FROM tblPasswordRecovery WHERE UserID = @UserId";


        private const string GET_ROLES = @"SELECT [Id], [Name]
                                           FROM tblRole";

        private const string GET_USERS_BY_ROLE = @"SELECT [Id], [Login], [Password], [FirstName], [Surname], [Email], [Phone] ,[Status]
                                                   FROM tblUser u 
                                                   INNER JOIN tblUserRoles r 
                                                   ON u.id=r.userId 
                                                   WHERE r.roleId = @RoleId
                                                   ORDER BY u.Id";

        private const string GET_USERS_BY_TEXT_FILTER = @"SELECT [Id], [Login], [Password], [FirstName], [Surname], [Email], [Phone] ,[Status]
                                                         FROM tblUser
	                                                     WHERE Login LIKE @text 
                                                         OR [Password] LIKE @text
			                                             OR Firstname LIKE @text
			                                             OR Surname LIKE @text
			                                             OR [Login] LIKE @text
			                                             OR Phone LIKE @text
			                                             OR Email LIKE @text";

        private const string GET_USER_ROLES = @"SELECT r.Id, r.Name 
                                                 FROM tblRole r 
                                                 INNER JOIN tblUserRoles ur
                                                 ON r.Id = ur.RoleId
                                                 WHERE ur.UserId = @UserId";

        private const string GET_ROLE_BY_NAME = @"SELECT Id, Name 
                                                  FROM tblRoles
                                                  WHERE Name=@Name;";

        private const string UPDATE_USER_STATUS = @"UPDATE tblUser
                                                    SET [Status] = @Status
                                                    WHERE Id = @UserId";

        private const string GET_USER_BY_LOGIN_PASSWORD = @"SELECT [Id], [Login], [Password], [FirstName], [Surname], [Email], [Phone] ,[Status]
                                               FROM tblUser WHERE [Login] = @Login AND [Password] = @Password";

        private const string DELETE_UNCONFIRMED_REGISTRATION = @"DELETE FROM tblUnconfirmedRegistration WHERE UserId=@userId;";

        private const string DELETE_PASSWORD_RECOVERY_TOKEN = @"DELETE FROM tblPasswordRecovery WHERE UserId=@userId;";

        private const string IS_USER_ADMINISTRATOR_OF_HOTEL = @"SELECT Id, UserId, HotelId FROM tblHotelAdministrators
                                                                WHERE UserId=@UserId AND HotelId=@HotelId";

        private const string IS_USER_ADMINISTRATOR_OF_EXCURSION = @"SELECT Id, UserId, ExcursionId FROM tblExcursionAdministrators
                                                                    WHERE UserId=@UserId AND ExcursionId=@ExcursionId";

        #endregion

        #region Fields

        private readonly string _connectionString;

        #endregion

        #region Constructors

        public UserRepository(string connStr)
        {
            _connectionString = connStr;
        }

        #endregion

        #region IUserRepository

        public List<User> GetUsers()
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_ALL_USERS, conn))
                {

                    using (var reader = cmd.ExecuteReader())
                    {
                        var users = new List<User>();
                        while (reader.Read())
                        {
                            var user = ReadUser(reader);
                            users.Add(user);
                        }

                        return users;
                    }
                }
            }
        }

        public User GetUserById(int id)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_USER_BY_ID, conn))
                {
                    cmd.Parameters.AddWithValue("@UserId", id);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var user = new User();
                        while (reader.Read())
                        {
                            user = ReadUser(reader);
                        }

                        return user;
                    }
                }
            }
        }

        public int GetUserIdByReservationId(int id)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_USER_ID_BY_RESERVATION_ID, conn))
                {
                    cmd.Parameters.AddWithValue("@ReservationId", id);

                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public User GetUserByLogin(string login)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_USER_BY_LOGIN, conn))
                {
                    cmd.Parameters.AddWithValue("@Login", login);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var user = new User();
                        while (reader.Read())
                        {
                            user = ReadUser(reader);
                        }

                        return (user.Login == null) ? null : user;
                    }
                }
            }
        }

        public User GetUserByEmail(string email)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_USER_BY_EMAIL, conn))
                {
                    cmd.Parameters.AddWithValue("@Email", email);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var user = new User();
                        while (reader.Read())
                        {
                            user = ReadUser(reader);
                        }

                        return (user.Email == null) ? null : user;
                    }
                }
            }
        }

        public User GetUserByPhone(string phone)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_USER_BY_PHONE, conn))
                {
                    cmd.Parameters.AddWithValue("@Phone", phone);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var user = new User();
                        while (reader.Read())
                        {
                            user = ReadUser(reader);
                        }

                        return (user.Phone == null) ? null : user;
                    }
                }
            }
        }

        public string GetUserRegistrationToken(int userId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_USER_REGISTRATION_TOKEN, conn))
                {
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        var token = (string)reader["RegistrationToken"];

                        return token;
                    }
                }
            }
        }

        public string GetUserPasswordRecoveryToken(int userId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_USER_PASSWORD_RECOVERY_TOKEN, conn))
                {
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        var token = (string)reader["PasswordRecoveryToken"];

                        return token;
                    }
                }
            }
        }

        public int InsertUser(User user)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(INSERT_USER, conn))
                {
                    cmd.Parameters.AddWithValue("Login", user.Login);
                    cmd.Parameters.AddWithValue("Password", user.Password);
                    cmd.Parameters.AddWithValue("Firstname", user.FirstName);
                    cmd.Parameters.AddWithValue("Surname", user.Surname);
                    cmd.Parameters.AddWithValue("Email", user.Email);
                    cmd.Parameters.AddWithValue("Phone", user.Phone);
                    cmd.Parameters.AddWithValue("Status", user.Status);

                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public void DeleteUser(User user)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(DELETE_USER_TABLE_USERROLES, conn))
                {
                    cmd.Parameters.AddWithValue("userId", user.Id);

                    cmd.ExecuteNonQuery();
                }
                using (var cmd = new SqlCommand(DELETE_UNCONFIRMED_REGISTRATION, conn))
                {
                    cmd.Parameters.AddWithValue("UserId", user.Id);

                    cmd.ExecuteNonQuery();
                }
                using (var cmd = new SqlCommand(DELETE_USER_TABLE_USER, conn))
                {
                    cmd.Parameters.AddWithValue("userId", user.Id);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void UpdateUser(User user)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(UPDATE_USER, conn))
                {
                    cmd.Parameters.AddWithValue("Login", user.Login);
                    cmd.Parameters.AddWithValue("Firstname", user.FirstName);
                    cmd.Parameters.AddWithValue("Surname", user.Surname);
                    cmd.Parameters.AddWithValue("Email", user.Email);
                    cmd.Parameters.AddWithValue("Phone", user.Phone);
                    cmd.Parameters.AddWithValue("Status", user.Status);
                    cmd.Parameters.AddWithValue("UserId", user.Id);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void UpdateUserPassword(User user)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(UPDATE_USER_PASSWORD, conn))
                {
                    cmd.Parameters.AddWithValue("Password", user.Password);
                    cmd.Parameters.AddWithValue("UserId", user.Id);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public int InserPasswordRecoveryRequest(int userId, string token, DateTime date)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(INSERT_PASSWORD_RECOVERY_REQUEST, conn))
                {
                    cmd.Parameters.AddWithValue("UserId", userId);
                    cmd.Parameters.AddWithValue("Token", token);
                    cmd.Parameters.AddWithValue("Time", date);

                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public int InsertUnconfirmedRegistration(int userId, string token, DateTime date)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(INSERT_UNCONFIRMED_REGISTRATION, conn))
                {
                    cmd.Parameters.AddWithValue("UserId", userId);
                    cmd.Parameters.AddWithValue("Token", token);
                    cmd.Parameters.AddWithValue("Time", date);

                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public List<Role> GetRoles()
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_ROLES, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var roles = new List<Role>();
                        while (reader.Read())
                        {
                            var role = ReadRole(reader);
                            roles.Add(role);
                        }

                        return roles;
                    }
                }
            }
        }

        public Role GetRoleByName(string name)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_ROLE_BY_NAME, conn))
                {
                    cmd.Parameters.AddWithValue("Name", name);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var role = ReadRole(reader);

                        return role;
                    }
                }
            }
        }

        public List<User> GetUsersByRole(int roleId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_USERS_BY_ROLE, conn))
                {
                    cmd.Parameters.AddWithValue("RoleId", roleId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var users = new List<User>();
                        while (reader.Read())
                        {
                            var user = ReadUser(reader);
                            users.Add(user);
                        }

                        return users;
                    }
                }
            }
        }

        public List<User> GetUsersByTextFilter(string text)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_USERS_BY_TEXT_FILTER, conn))
                {
                    cmd.Parameters.AddWithValue("text", "%" + text + "%");
                    using (var reader = cmd.ExecuteReader())
                    {
                        var users = new List<User>();
                        while (reader.Read())
                        {
                            var user = ReadUser(reader);
                            users.Add(user);
                        }

                        return users;
                    }
                }
            }
        }

        public void SetRoles(int userId, bool isUser = false, bool isAdmin = false, bool isHotelAdmin = false, bool isCafeAdmin = false, bool isExcursionAdmin = false)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand("spUpdateRoles", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("UserId", userId);
                    cmd.Parameters.AddWithValue("User", isUser);
                    cmd.Parameters.AddWithValue("SA", isAdmin);
                    cmd.Parameters.AddWithValue("HA", isHotelAdmin);
                    cmd.Parameters.AddWithValue("CA", isCafeAdmin);
                    cmd.Parameters.AddWithValue("EA", isExcursionAdmin);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<Role> GetUserRoles(int userId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_USER_ROLES, conn))
                {
                    cmd.Parameters.AddWithValue("UserId", userId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var roles = new List<Role>();
                        while (reader.Read())
                        {
                            var role = ReadRole(reader);
                            roles.Add(role);
                        }
                        return roles;
                    }
                }
            }
        }

        public void UpdateUserStatus(int userId, bool status)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(UPDATE_USER_STATUS, conn))
                {
                    cmd.Parameters.AddWithValue("UserId", userId);
                    cmd.Parameters.AddWithValue("Status", status);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public User GetUser(string login, string password)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_USER_BY_LOGIN_PASSWORD, conn))
                {
                    cmd.Parameters.AddWithValue("@Login", login);
                    cmd.Parameters.AddWithValue("@Password", password);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var user = new User();
                        while (reader.Read())
                        {
                            user = ReadUser(reader);
                        }

                        return user;
                    }
                }
            }
        }

        public void DeleteUnconfirmRegistration(int userId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(DELETE_UNCONFIRMED_REGISTRATION, conn))
                {
                    cmd.Parameters.AddWithValue("UserId", userId);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeletePasswordRecoveryToken(int userId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(DELETE_PASSWORD_RECOVERY_TOKEN, conn))
                {
                    cmd.Parameters.AddWithValue("UserId", userId);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void InsertUserFeedback(string login, string email, string message)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(INSERT_USER_FEEDBACK, conn))
                {
                    cmd.Parameters.AddWithValue("@Login", login);
                    cmd.Parameters.AddWithValue("@Email", email);
                    cmd.Parameters.AddWithValue("@Message", message);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public int IsUserAdministratorOfHotel(int userId, int hotelId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(IS_USER_ADMINISTRATOR_OF_HOTEL, conn))
                {
                    cmd.Parameters.AddWithValue("UserId", userId);
                    cmd.Parameters.AddWithValue("HotelId", hotelId);

                    using (var reader = cmd.ExecuteReader())
                    {
                        var res = 0;
                        while (reader.Read())
                        {
                            res = 1;
                        }

                        return res;
                    }
                }
            }
        }

        public int IsUserAdministratorOfExcursion(int userId, int excursionId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(IS_USER_ADMINISTRATOR_OF_EXCURSION, conn))
                {
                    cmd.Parameters.AddWithValue("UserId", userId);
                    cmd.Parameters.AddWithValue("ExcursionId", excursionId);

                    using (var reader = cmd.ExecuteReader())
                    {
                        var res = 0;
                        while (reader.Read())
                        {
                            res = 1;
                        }

                        return res;
                    }
                }
            }
        }

        #endregion

        #region Helpers

        private User ReadUser(SqlDataReader reader)
        {
            var user = new User
            {
                Id = (int)reader["Id"],
                Login = (string)reader["Login"],
                Password = (string)reader["Password"],
                FirstName = (string)reader["FirstName"],
                Surname = (string)reader["Surname"],
                Email = (string)reader["Email"],
                Phone = (string)reader["Phone"],
                Status = (bool)reader["Status"]
            };
            return user;
        }

        private Role ReadRole(SqlDataReader reader)
        {
            var role = new Role
            {
                Id = (int)reader["Id"],
                Name = (string)reader["Name"]
            };
            return role;
        }

        #endregion
    }
}
