﻿using System;
using System.Collections.Generic;
using CityCenter.Repositories.Abstract;
using System.Data.SqlClient;
using CityCenter.Entities;
using System.Data;

namespace CityCenter.Repositories.Sql
{
    public class HotelRepository : IHotelRepository
    {
        #region Queries

        private const string SP_BOOK_ROOM = "spBookRoom";

        private const string GET_HOTEL_ROOM_BY_HOTEL_ID = @"SELECT [Id], [HotelId], [RoomType], [RoomsCount], [Price] FROM tblHotelRooms WHERE HotelId = @HotelId";

        private const string GET_AVAILABLE_HOTEL_ROOM_BY_HOTEL_ID = @"SELECT [Id], [HotelId], [RoomType], [RoomsCount], [Price] FROM tblHotelRooms r
                                                                      WHERE r.HotelId = @HotelId AND dbo.IsRoomAvailable(r.Id, @BeginDate, @EndDate) = 1";

        private const string GET_HOTEL_ROOMS_ID = @"SELECT Id FROM tblHotelRooms
                                                 WHERE HotelId = @HotelId AND RoomType = @RoomType";

        private const string GET_HOTELS = @"SELECT [Id], [Name], [Address], [MapCoords], [Phones], [Rating], [Parking], [Pool], [Sauna], [TennisCourt], [Gym]
                                            FROM tblHotel";

        private const string GET_HOTEL_DESCRIPTIONS_PAGING = @"SELECT [HotelId], [Description], [Header], [Photo], [PhotoExtension], [Phone], [Address], [Site]
                                                               FROM (SELECT ROW_NUMBER() OVER(ORDER BY HotelId) AS NUMBER, * FROM tblHotelDescription ) AS TBL
                                                               WHERE NUMBER BETWEEN ((@PageNumber - 1) * @PageSize + 1) AND (@PageNumber * @PageSize)
                                                               ORDER BY HotelId";

        private const string GET_HOTEL_RESERVATIONS_DETAILS_BY_USERID_PAGING = @"SELECT Id, HotelId, Name, RoomType, Price, BeginDate, EndDate, Parking, Pool, Sauna, TennisCourt, Gym, Status
                                                                                FROM (SELECT ROW_NUMBER() OVER(ORDER BY res.BeginDate) AS NUMBER, res.Id AS Id, h.Id As HotelId, h.Name AS Name, room.RoomType AS RoomType, room.Price AS Price, res.BeginDate AS BeginDate, res.EndDate AS EndDate, res.Parking AS Parking, res.Pool AS Pool, res.Sauna AS Sauna, res.TennisCourt AS TennisCourt, res.Gym AS Gym, res.Status AS Status FROM tblHotelReservation res
                                                                                INNER JOIN tblHotelRooms room
                                                                                ON res.HotelRoomsId = room.Id
                                                                                INNER JOIN tblHotel h
                                                                                ON room.HotelId = h.Id
                                                                                WHERE res.UserId = @UserId) AS TBL
                                                                                WHERE NUMBER BETWEEN ((@PageNumber - 1) * @PageSize + 1) AND (@PageNumber * @PageSize)";

        private const string GET_PENDING_HOTEL_RESERVATIONS_DETAILS_BY_HOTELID_PAGING = @"SELECT Id, HotelId, Name, RoomType, Price, BeginDate, EndDate, Parking, Pool, Sauna, TennisCourt, Gym, Status
                                                                                FROM (SELECT ROW_NUMBER() OVER(ORDER BY res.BeginDate) AS NUMBER, res.Id AS Id, h.Id As HotelId, h.Name AS Name, room.RoomType AS RoomType, room.Price AS Price, res.BeginDate AS BeginDate, res.EndDate AS EndDate, res.Parking AS Parking, res.Pool AS Pool, res.Sauna AS Sauna, res.TennisCourt AS TennisCourt, res.Gym AS Gym, res.Status AS Status FROM tblHotelReservation res
                                                                                INNER JOIN tblHotelRooms room
                                                                                ON res.HotelRoomsId = room.Id
                                                                                INNER JOIN tblHotel h
                                                                                ON room.HotelId = h.Id
                                                                                WHERE h.Id = @HotelId AND res.Status = @Status) AS TBL
                                                                                WHERE NUMBER BETWEEN ((@PageNumber - 1) * @PageSize + 1) AND (@PageNumber * @PageSize)";

        private const string GET_PENDING_RESERVATION_COUNT_BY_HOTELID = @"SELECT COUNT(*)
                                                                 FROM tblHotelReservation res
                                                                 INNER JOIN tblHotelRooms room
                                                                    On res.HotelRoomsId = room.Id
                                                                 INNER JOIN tblHotel h
                                                                    ON room.HotelId = h.Id
                                                                 WHERE h.Id = @HotelId AND res.Status = @Status";

        private const string GET_HOTEL_RESERVATIONS_DETAILS_BY_HOTELID_PAGING = @"SELECT Id, HotelId, Name, RoomType, Price, BeginDate, EndDate, Parking, Pool, Sauna, TennisCourt, Gym, Status
                                                                                FROM (SELECT ROW_NUMBER() OVER(ORDER BY res.BeginDate) AS NUMBER, res.Id AS Id, h.Id As HotelId, h.Name AS Name, room.RoomType AS RoomType, room.Price AS Price, res.BeginDate AS BeginDate, res.EndDate AS EndDate, res.Parking AS Parking, res.Pool AS Pool, res.Sauna AS Sauna, res.TennisCourt AS TennisCourt, res.Gym AS Gym, res.Status AS Status FROM tblHotelReservation res
                                                                                INNER JOIN tblHotelRooms room
                                                                                ON res.HotelRoomsId = room.Id
                                                                                INNER JOIN tblHotel h
                                                                                ON room.HotelId = h.Id
                                                                                WHERE h.Id = @HotelId AND res.Status != @Status) AS TBL
                                                                                WHERE NUMBER BETWEEN ((@PageNumber - 1) * @PageSize + 1) AND (@PageNumber * @PageSize)";

        private const string GET_RESERVATION_COUNT_BY_HOTELID = @"SELECT COUNT(*)
                                                                 FROM tblHotelReservation res
                                                                 INNER JOIN tblHotelRooms room
                                                                    On res.HotelRoomsId = room.Id
                                                                 INNER JOIN tblHotel h
                                                                    ON room.HotelId = h.Id
                                                                 WHERE h.Id = @HotelId AND res.Status != @Status";

        private const string GET_HOTEL_DESCRIPTIONS_BY_ID = @"SELECT [HotelId], [Description], [Header], [Photo], [PhotoExtension], [Phone], [Address], [Site]
                                                              FROM tblHotelDescription WHERE HotelId=@HotelId";

        private const string GET_HOTEL_BY_ID = @"SELECT [Id], [Name], [Address], [MapCoords], [Phones], [Rating], [Parking], [Pool], [Sauna], [TennisCourt], [Gym]
                                                 FROM tblHotel WHERE Id = @HotelId";

        private const string GET_HOTEL_BY_HOTEL_NAME = @"SELECT [Id], [Name], [Address], [MapCoords], [Phones], [Rating], [Parking], [Pool], [Sauna], [TennisCourt], [Gym]
                                                 FROM tblHotel WHERE Name = @HotelName";

        private const string GET_HOTEL_DESCRIPTIONS = @"SELECT [HotelId], [Description], [Header], [Photo], [PhotoExtension], [Phone], [Address], [Site]
                                                        FROM tblHotelDescription";

        private const string UPDATE_HOTEL_DESCRIPTION = @"UPDATE tblHotelDescription
                                                          SET Description=@Description, Header=@Header, Phone=@Phone, Address=@Address, Site=@Site
                                                          WHERE HotelId=@HotelId;";

        private const string UPDATE_HOTEL_NAME = @"UPDATE tblHotel
                                                SET Name=@Name
                                                WHERE Id=@HotelId;";

        private const string GET_HOTEL_DESCRIPTIONS_COUNT = @"SELECT COUNT(*) FROM tblHotelDescription";

        private const string GET_RESERVATION_COUNT = @"SELECT COUNT(*) FROM tblHotelReservation
                                                        WHERE UserId=@UserId";

        private const string GET_HOTEL_RESERVATIONS_DETAILS_BY_USERID = @"SELECT res.Id AS Id, h.Id As HotelId, h.Name AS Name, room.RoomType AS RoomType, room.Price AS Price, res.BeginDate AS BeginDate, res.EndDate AS EndDate, res.Parking AS Parking, res.Pool AS Pool, res.Sauna AS Sauna, res.TennisCourt AS TennisCourt, res.Gym AS Gym, res.Status AS Status
                                                                          FROM tblHotelReservation res
                                                                          INNER JOIN tblHotelRooms room
                                                                             On res.HotelRoomsId = room.Id
                                                                          INNER JOIN tblHotel h
                                                                             ON room.HotelId = h.Id
                                                                          WHERE res.UserId = @UserId";

        private const string UPDATE_HOTEL_ROOM = @"UPDATE [dbo].[tblHotelRooms]
                                                   SET [HotelId] = @HotelId, [RoomType] = @RoomType, [RoomsCount] = @RoomsCount, [Price] = @Price
                                                   WHERE Id = @Id";

        private const string INSERT_HOTEL_ROOM = @"INSERT INTO tblHotelRooms ([HotelId], [RoomType], [RoomsCount], [Price])
                                                   VALUES (@HotelId, @RoomType, @RoomsCount, @Price) SELECT CAST(SCOPE_IDENTITY() AS INT);";

        private const string DELETE_HOTEL_ROOM = @"DELETE FROM tblHotelRooms WHERE Id=@Id;";

        private const string GET_HOTELS_BY_ADMIN_ID = @"SELECT h.* FROM tblHotel h 
                                                       INNER JOIN tblHotelAdministrators a
                                                       ON h.id = a.HotelId
                                                       WHERE a.UserId = @AdminId";

        private const string GET_HOTEL_RESERVATIONS_DETAILS_BY_HOTELID = @"SELECT res.Id AS Id, h.Id As HotelId, h.Name AS Name, room.RoomType AS RoomType, room.Price AS Price, res.BeginDate AS BeginDate, res.EndDate AS EndDate, res.Parking AS Parking, res.Pool AS Pool, res.Sauna AS Sauna, res.TennisCourt AS TennisCourt, res.Gym AS Gym, res.Status AS Status
                                                                          FROM tblHotelReservation res
                                                                          INNER JOIN tblHotelRooms room
                                                                             On res.HotelRoomsId = room.Id
                                                                          INNER JOIN tblHotel h
                                                                             ON room.HotelId = h.Id
                                                                          WHERE h.Id = @HotelId";

        private const string UPDATE_HOTEL_PHOTO = @"UPDATE tblHotelDescription
                                                    SET Photo=@Photo, PhotoExtension=@PhotoExtension
                                                    WHERE HotelId=@HotelId;";

        private const string CHANGE_RESERVATION_STATUS = @"UPDATE tblHotelReservation
                                                    SET Status=@Status
                                                    WHERE Id=@ReservationId;";

        private const string GET_TODAY_PENDING_RESERVATIONS = @"SELECT res.Id AS Id, h.Id As HotelId, h.Name AS Name, room.RoomType AS RoomType, room.Price AS Price, res.BeginDate AS BeginDate, res.EndDate AS EndDate, res.Parking AS Parking, res.Pool AS Pool, res.Sauna AS Sauna, res.TennisCourt AS TennisCourt, res.Gym AS Gym, res.Status AS Status 
                                                                FROM tblHotelReservation res
                                                                INNER JOIN tblHotelRooms room
                                                                ON res.HotelRoomsId = room.Id
                                                                INNER JOIN tblHotel h
                                                                ON room.HotelId = h.Id
                                                                WHERE res.BeginDate <= GETDATE() AND res.Status = 0";

        private const string INSERT_NEW_HOTEL = @"INSERT INTO tblHotel (Name, Address, MapCoords, Phones, Rating, Parking, Pool, Sauna, TennisCourt, Gym)
                                                  VALUES (@Name, @Address, @MapCoords, @Phones, @Rating, @Parking, @Pool, @Sauna, @TennisCourt, @Gym);
                                                  SELECT CAST(SCOPE_IDENTITY() AS INT);";

        private const string INSERT_NEW_HOTEL_DESCRIPTION = @"INSERT INTO tblHotelDescription ([HotelId], [Description], [Header], [Photo], PhotoExtension, [Phone], [Address], [Site])
                                                              VALUES (@HotelId, @Description, @Header, @Photo, @PhotoExtension, @Phone, @Address, @Site)";

        private const string INSERT_NEW_HOTEL_ADMIN = @"INSERT INTO tblHotelAdministrators ([UserId], [HotelId])
                                                        VALUES (@UserId, @HotelId)";

        #endregion

        #region Fields

        private readonly string _connectionString;

        #endregion

        #region Constructors

        public HotelRepository(string connStr)
        {
            _connectionString = connStr;
        }

        #endregion

        #region IHotelRepository

        public int BookRoom(int userId, int hotelRoomsId, string beginDate, string endDate, bool parking, bool pool, bool sauna, bool tennisCourt, bool gym, int status)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(SP_BOOK_ROOM, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("UserId", userId);
                    cmd.Parameters.AddWithValue("HotelRoomsId", hotelRoomsId);
                    cmd.Parameters.AddWithValue("BeginDate", beginDate);
                    cmd.Parameters.AddWithValue("EndDate", endDate);
                    cmd.Parameters.AddWithValue("Parking", parking);
                    cmd.Parameters.AddWithValue("Pool", pool);
                    cmd.Parameters.AddWithValue("Sauna", sauna);
                    cmd.Parameters.AddWithValue("TennisCourt", tennisCourt);
                    cmd.Parameters.AddWithValue("Gym", gym);
                    cmd.Parameters.AddWithValue("Status", status);
                    var outputIdParam = new SqlParameter("@Id", SqlDbType.Int)
                    {
                        Direction = ParameterDirection.Output
                    };
                    cmd.Parameters.Add(outputIdParam);
                    cmd.ExecuteNonQuery();
                    return (int)outputIdParam.Value;
                }
            }
        }

        public List<HotelRooms> GetHotelRoomTypes(int hotelId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_HOTEL_ROOM_BY_HOTEL_ID, conn))
                {
                    cmd.Parameters.AddWithValue("HotelId", hotelId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var hotelRoomTypes = new List<HotelRooms>();
                        while (reader.Read())
                        {
                            hotelRoomTypes.Add(ReadHotelRoom(reader));
                        }
                        return hotelRoomTypes.Count == 0 ? null : hotelRoomTypes;
                    }
                }
            }
        }

        public List<HotelRooms> GetAvailableHotelRoomTypes(int hotelId, string beginDate, string endDate)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_AVAILABLE_HOTEL_ROOM_BY_HOTEL_ID, conn))
                {
                    cmd.Parameters.AddWithValue("HotelId", hotelId);
                    cmd.Parameters.AddWithValue("BeginDate", beginDate);
                    cmd.Parameters.AddWithValue("EndDate", endDate);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var hotelRoomTypes = new List<HotelRooms>();
                        while (reader.Read())
                        {
                            hotelRoomTypes.Add(ReadHotelRoom(reader));
                        }
                        return hotelRoomTypes.Count == 0 ? null : hotelRoomTypes;
                    }
                }
            }
        }

        public int GetHotelRoomsId(int hotelId, string roomType)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_HOTEL_ROOMS_ID, conn))
                {
                    cmd.Parameters.AddWithValue("HotelId", hotelId);
                    cmd.Parameters.AddWithValue("RoomType", roomType);

                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public List<Hotel> GetHotels()
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_HOTELS, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var hotels = new List<Hotel>();
                        while (reader.Read())
                        {
                            hotels.Add(ReadHotel(reader));
                        }
                        return hotels;
                    }
                }
            }
        }

        public List<HotelDescription> GetHotelDescriptions()
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_HOTEL_DESCRIPTIONS, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var hotelDescriptions = new List<HotelDescription>();
                        while (reader.Read())
                        {
                            hotelDescriptions.Add(ReadHotelDescription(reader));
                        }
                        return hotelDescriptions.Count != 0 ? hotelDescriptions : null;
                    }
                }
            }
        }

        public List<HotelDescription> GetHotelDescriptions(int pageNumber, int pageSize)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_HOTEL_DESCRIPTIONS_PAGING, conn))
                {
                    cmd.Parameters.AddWithValue("@PageSize", pageSize);
                    cmd.Parameters.AddWithValue("@PageNumber", pageNumber);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var hotelDescriptions = new List<HotelDescription>();
                        while (reader.Read())
                        {
                            hotelDescriptions.Add(ReadHotelDescription(reader));
                        }
                        return hotelDescriptions.Count != 0 ? hotelDescriptions : null;
                    }
                }
            }
        }

        public void UpdateHotelDescription(HotelDescription hotelDescription)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(UPDATE_HOTEL_DESCRIPTION, conn))
                {
                    cmd.Parameters.AddWithValue("Description", hotelDescription.Description);
                    cmd.Parameters.AddWithValue("Header", hotelDescription.Header);
                    cmd.Parameters.AddWithValue("Phone", hotelDescription.Phone);
                    cmd.Parameters.AddWithValue("Address", hotelDescription.Address);
                    cmd.Parameters.AddWithValue("Site", hotelDescription.Site);
                    cmd.Parameters.AddWithValue("HotelId", hotelDescription.HotelId);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void UpdateHotelName(string name, int id)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(UPDATE_HOTEL_NAME, conn))
                {
                    cmd.Parameters.AddWithValue("Name", name);
                    cmd.Parameters.AddWithValue("HotelId", id);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public HotelDescription GetHotelDescriptionById(int hotelId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_HOTEL_DESCRIPTIONS_BY_ID, conn))
                {
                    cmd.Parameters.AddWithValue("@HotelId", hotelId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var hotelDescription = new HotelDescription();
                        while (reader.Read())
                        {
                            hotelDescription = ReadHotelDescription(reader);
                        }

                        return (hotelDescription.Header == null) ? null : hotelDescription;
                    }
                }
            }
        }

        public Hotel GetHotelById(int id)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_HOTEL_BY_ID, conn))
                {
                    cmd.Parameters.AddWithValue("@HotelId", id);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var hotel = new Hotel();
                        while (reader.Read())
                        {
                            hotel = ReadHotel(reader);
                        }

                        return (hotel.Name == null) ? null : hotel;
                    }
                }
            }
        }

        public Hotel GetHotelByHotelName(string hotelName)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_HOTEL_BY_HOTEL_NAME, conn))
                {
                    cmd.Parameters.AddWithValue("@HotelName", hotelName);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var hotel = new Hotel();
                        while (reader.Read())
                        {
                            hotel = ReadHotel(reader);
                        }

                        return (hotel.Name == null) ? null : hotel;
                    }
                }
            }
        }

        public int GetHotelDescriptionsCount()
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_HOTEL_DESCRIPTIONS_COUNT, conn))
                {
                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public int GetReservationCount(int userId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_RESERVATION_COUNT, conn))
                {
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public int GetPendingReservationCountByHotelId(int hotelId, int status)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_PENDING_RESERVATION_COUNT_BY_HOTELID, conn))
                {
                    cmd.Parameters.AddWithValue("@HotelId", hotelId);
                    cmd.Parameters.AddWithValue("@Status", status);
                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public int GetReservationCountByHotelId(int hotelId, int status)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_RESERVATION_COUNT_BY_HOTELID, conn))
                {
                    cmd.Parameters.AddWithValue("@HotelId", hotelId);
                    cmd.Parameters.AddWithValue("@Status", status);
                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public List<HotelReservationDetails> GetHotelReservationsByUserId(int userId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_HOTEL_RESERVATIONS_DETAILS_BY_USERID, conn))
                {
                    cmd.Parameters.AddWithValue("UserId", userId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var reservations = new List<HotelReservationDetails>();
                        while (reader.Read())
                        {
                            reservations.Add(ReadHotelReservationDetails(reader));
                        }
                        return reservations.Count != 0 ? reservations : null;
                    }
                }
            }
        }

        public List<HotelReservationDetails> GetHotelReservationsByUserId(int userId, int pageNumber, int pageSize)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_HOTEL_RESERVATIONS_DETAILS_BY_USERID_PAGING, conn))
                {
                    cmd.Parameters.AddWithValue("UserId", userId);
                    cmd.Parameters.AddWithValue("@PageSize", pageSize);
                    cmd.Parameters.AddWithValue("@PageNumber", pageNumber);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var reservations = new List<HotelReservationDetails>();
                        while (reader.Read())
                        {
                            reservations.Add(ReadHotelReservationDetails(reader));
                        }
                        return reservations;
                    }
                }
            }
        }

        public void UpdateHotelRoom(HotelRooms room)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(UPDATE_HOTEL_ROOM, conn))
                {
                    cmd.Parameters.AddWithValue("Id", room.Id);
                    cmd.Parameters.AddWithValue("Price", room.Price);
                    cmd.Parameters.AddWithValue("RoomType", room.RoomType);
                    cmd.Parameters.AddWithValue("RoomsCount", room.RoomsCount);
                    cmd.Parameters.AddWithValue("HotelId", room.HotelId);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public int InsertHotelRoom(HotelRooms room)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(INSERT_HOTEL_ROOM, conn))
                {
                    cmd.Parameters.AddWithValue("HotelId", room.HotelId);
                    cmd.Parameters.AddWithValue("RoomType", room.RoomType);
                    cmd.Parameters.AddWithValue("RoomsCount", room.RoomsCount);
                    cmd.Parameters.AddWithValue("Price", room.Price);

                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public void DeletetHotelRoom(HotelRooms room)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(DELETE_HOTEL_ROOM, conn))
                {
                    cmd.Parameters.AddWithValue("Id", room.Id);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<Hotel> GetHotelsByAdminId(int adminId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_HOTELS_BY_ADMIN_ID, conn))
                {
                    cmd.Parameters.AddWithValue("AdminId", adminId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var hotels = new List<Hotel>();
                        while (reader.Read())
                        {
                            hotels.Add(ReadHotel(reader));
                        }
                        return hotels;
                    }
                }
            }
        }

        public List<HotelReservationDetails> GetHotelReservationByHotelId(int hotelId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_HOTEL_RESERVATIONS_DETAILS_BY_HOTELID, conn))
                {
                    cmd.Parameters.AddWithValue("HotelId", hotelId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var reservations = new List<HotelReservationDetails>();
                        while (reader.Read())
                        {
                            reservations.Add(ReadHotelReservationDetails(reader));
                        }
                        return reservations.Count != 0 ? reservations : null;
                    }
                }
            }
        }

        public List<HotelReservationDetails> GetPendingHotelReservationByHotelId(int hotelId, int pageNumber, int pageSize, int status)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_PENDING_HOTEL_RESERVATIONS_DETAILS_BY_HOTELID_PAGING, conn))
                {
                    cmd.Parameters.AddWithValue("HotelId", hotelId);
                    cmd.Parameters.AddWithValue("Status", status);
                    cmd.Parameters.AddWithValue("@PageSize", pageSize);
                    cmd.Parameters.AddWithValue("@PageNumber", pageNumber);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var reservations = new List<HotelReservationDetails>();
                        while (reader.Read())
                        {
                            reservations.Add(ReadHotelReservationDetails(reader));
                        }
                        return reservations;
                    }
                }
            }
        }

        public List<HotelReservationDetails> GetHotelReservationByHotelId(int hotelId, int pageNumber, int pageSize, int status)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_HOTEL_RESERVATIONS_DETAILS_BY_HOTELID_PAGING, conn))
                {
                    cmd.Parameters.AddWithValue("HotelId", hotelId);
                    cmd.Parameters.AddWithValue("Status", status);
                    cmd.Parameters.AddWithValue("@PageSize", pageSize);
                    cmd.Parameters.AddWithValue("@PageNumber", pageNumber);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var reservations = new List<HotelReservationDetails>();
                        while (reader.Read())
                        {
                            reservations.Add(ReadHotelReservationDetails(reader));
                        }
                        return reservations;
                    }
                }
            }
        }

        public void UpdateHotelImage(HotelDescription hotelDescription)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(UPDATE_HOTEL_PHOTO, conn))
                {
                    cmd.Parameters.AddWithValue("Photo", hotelDescription.Photo);
                    cmd.Parameters.AddWithValue("PhotoExtension", hotelDescription.PhotoExtension);
                    cmd.Parameters.AddWithValue("HotelId", hotelDescription.HotelId);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void ChangeReservationStatus(int reservationId, int status)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(CHANGE_RESERVATION_STATUS, conn))
                {
                    cmd.Parameters.AddWithValue("ReservationId", reservationId);
                    cmd.Parameters.AddWithValue("Status", status);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public int InsertHotel(Hotel hotel)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(INSERT_NEW_HOTEL, conn))
                {
                    cmd.Parameters.AddWithValue("Name", hotel.Name);
                    cmd.Parameters.AddWithValue("Address", hotel.Address);
                    cmd.Parameters.AddWithValue("MapCoords", hotel.MapCoords);
                    cmd.Parameters.AddWithValue("Phones", hotel.Phones);
                    cmd.Parameters.AddWithValue("Rating", hotel.Rating);
                    cmd.Parameters.AddWithValue("Parking", hotel.Parking);
                    cmd.Parameters.AddWithValue("Pool", hotel.Pool);
                    cmd.Parameters.AddWithValue("Sauna", hotel.Sauna);
                    cmd.Parameters.AddWithValue("TennisCourt", hotel.TennisCourt);
                    cmd.Parameters.AddWithValue("Gym", hotel.Gym);

                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        public void InsertNewHotelAdmin(int hotelId, int userId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(INSERT_NEW_HOTEL_ADMIN, conn))
                {
                    cmd.Parameters.AddWithValue("HotelId", hotelId);
                    cmd.Parameters.AddWithValue("UserId", userId);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void InsertHotelDescription(HotelDescription description)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(INSERT_NEW_HOTEL_DESCRIPTION, conn))
                {
                    cmd.Parameters.AddWithValue("HotelId", description.HotelId);
                    cmd.Parameters.AddWithValue("Header", description.Header);
                    cmd.Parameters.AddWithValue("Description", description.Description);
                    cmd.Parameters.AddWithValue("Photo", description.Photo);
                    cmd.Parameters.AddWithValue("PhotoExtension", description.PhotoExtension);
                    cmd.Parameters.AddWithValue("Phone", description.Phone);
                    cmd.Parameters.AddWithValue("Address", description.Address);
                    cmd.Parameters.AddWithValue("Site", description.Site);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<HotelReservationDetails> GetTodayPendingReservations()
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(GET_TODAY_PENDING_RESERVATIONS, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var reservations = new List<HotelReservationDetails>();
                        while (reader.Read())
                        {
                            reservations.Add(ReadHotelReservationDetails(reader));
                        }
                        return reservations;
                    }
                }
            }
        }

        #endregion

        #region Helpers

        private Hotel ReadHotel(SqlDataReader reader)
        {
            var hotel = new Hotel
            {
                Id = (int)reader["Id"],
                Name = (string)reader["Name"],
                Address = (string)reader["Address"],
                MapCoords = (string)reader["MapCoords"],
                Phones = (string)reader["Phones"],
                Rating = (int)reader["Rating"],
                Parking = (bool)reader["Parking"],
                Pool = (bool)reader["Pool"],
                Sauna = (bool)reader["Sauna"],
                TennisCourt = (bool)reader["TennisCourt"],
                Gym = (bool)reader["Gym"]
            };

            return hotel;
        }

        private HotelRooms ReadHotelRoom(SqlDataReader reader)
        {
            var hotel = new HotelRooms
            {
                Id = (int)reader["Id"],
                HotelId = (int)reader["HotelId"],
                RoomType = (string)reader["RoomType"],
                RoomsCount = (int)reader["RoomsCount"],
                Price = (decimal)reader["Price"]
            };

            return hotel;
        }

        private HotelDescription ReadHotelDescription(SqlDataReader reader)
        {
            var description = new HotelDescription
            {
                HotelId = (int)reader["HotelId"],
                Description = (string)reader["Description"],
                Header = (string)reader["Header"],
                Photo = (byte[])reader["Photo"],
                PhotoExtension = (string)reader["PhotoExtension"],
                Phone = (string)reader["Phone"],
                Address = (string)reader["Address"],
                Site = (string)reader["Site"]
            };

            return description;
        }

        private HotelReservationDetails ReadHotelReservationDetails(SqlDataReader reader)
        {
            var reservation = new HotelReservationDetails()
            {
                Id = (int)reader["Id"],
                HotelId = (int)reader["HotelId"],
                HotelName = (string)reader["Name"],
                RoomType = (string)reader["RoomType"],
                Price = (decimal)reader["Price"],
                BeginDate = (DateTime)reader["BeginDate"],
                EndDate = (DateTime)reader["EndDate"],
                Parking = (bool)reader["Parking"],
                Pool = (bool)reader["Pool"],
                Sauna = (bool)reader["Sauna"],
                TennisCourt = (bool)reader["TennisCourt"],
                Gym = (bool)reader["Gym"],
                Status = (int)reader["Status"]
            };

            return reservation;
        }


        #endregion

    }
}
