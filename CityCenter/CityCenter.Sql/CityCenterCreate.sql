CREATE DATABASE CityCenter

GO

USE CityCenter

GO

CREATE TABLE dbo.tblRole
(
	Id           INT IDENTITY(1, 1) NOT NULL, 
	Name		 NVARCHAR(64) NOT NULL
	
	CONSTRAINT PK_tblRole_Id PRIMARY KEY(Id),
)

CREATE TABLE dbo.tblUser 
( 
	Id           INT IDENTITY(1, 1) NOT NULL, 
	[Login]      NVARCHAR(16) NOT NULL, 
	[Password]   NVARCHAR(256) NOT NULL, 
	FirstName    NVARCHAR(64) NOT NULL, 
	Surname      NVARCHAR(64) NOT NULL, 
	Email        NVARCHAR(64) NOT NULL, 
	Phone        NVARCHAR(64) NOT NULL, 
	[Status]	     BIT NOT NULL,

	CONSTRAINT PK_tblUser_Id PRIMARY KEY(Id),
	CONSTRAINT UQ_tblUser_Login UNIQUE([Login]),
	CONSTRAINT UQ_tblUser_Email UNIQUE(Email),
	CONSTRAINT UQ_tblUser_Phone UNIQUE(Phone),
	CONSTRAINT CK_tblUser_Email CHECK (Email LIKE '%@%.%'),
	CONSTRAINT CK_tblUser_Phone CHECK (Phone LIKE '+[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')
);

CREATE TABLE dbo.tblUnconfirmedRegistration(
 Id      INT IDENTITY(1, 1) NOT NULL,
 UserId           INT NOT NULL,
 RegistrationToken NVARCHAR(48) NOT NULL,
 RegistrationTime DATETIME

 CONSTRAINT FK_tblUnconfirmedRegistration_UserId_tblUser_Id FOREIGN KEY(UserId) REFERENCES tblUser(Id) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT UQ_tblUnconfirmedRegistration_UserId UNIQUE(UserId)
);

CREATE TABLE dbo.tblPasswordRecovery(
 Id      INT IDENTITY(1, 1) NOT NULL,
 UserId           INT NOT NULL,
 PasswordRecoveryToken NVARCHAR(48) NOT NULL,
 PasswordRecoveryTime DATETIME

 CONSTRAINT FK_tblPasswordRecovery_UserId_tblUser_Id FOREIGN KEY(UserId) REFERENCES tblUser(Id) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT UQ_tblPasswordRecovery_UserId UNIQUE(UserId)
);

CREATE TABLE dbo.tblUserRoles 
(
	Id 				 INT IDENTITY(1, 1) NOT NULL,
	UserId           INT,
	RoleId			 INT,
	
	CONSTRAINT PK_tblUserRoles_Id PRIMARY KEY(Id),
	CONSTRAINT FK_tblUserRoles_UserId_tblUser_Id FOREIGN KEY(UserId) REFERENCES tblUser(Id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT FK_tblUserRoles_RoleId_tblRole_Id FOREIGN KEY(RoleId) REFERENCES tblRole(Id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE dbo.tblHotel
(
    [Id]        INT IDENTITY(1, 1) NOT NULL,
 [Name]      NVARCHAR(64) NOT NULL,
 [Address]   NVARCHAR(64) NOT NULL,
 [MapCoords] NVARCHAR(22) NOT NULL,
 [Phones]    NVARCHAR(64) NOT NULL,
 [Rating]    INT NOT NULL,
 [Parking]   BIT NOT NULL,
 [Pool]      BIT NOT NULL,
 [Sauna]     BIT NOT NULL,
 [TennisCourt] BIT NOT NULL,
 [Gym]       BIT NOT NULL,

    CONSTRAINT  PK_tblHotel_Id PRIMARY KEY(Id)
)

CREATE TABLE dbo.tblHotelRooms
(
 [Id]        INT IDENTITY(1, 1) NOT NULL,
 [HotelId]    INT NOT NULL,
 [RoomType] NVARCHAR(24) NOT NULL,
 [RoomsCount] INT NOT NULL,
 [Price] DECIMAL NOT NULL,

 CONSTRAINT  PK_tblHotelRooms_Id PRIMARY KEY(Id),
 CONSTRAINT  FK_tblHotelRooms_HotelId_tblHotel_Id FOREIGN KEY(HotelId) REFERENCES tblHotel(Id) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT  UQ_tblHotelRooms_HotelId_RoomType UNIQUE([HotelId], [RoomType])
)

CREATE TABLE dbo.tblHotelDescription
(
 [HotelId]		 INT NOT NULL,
 [Description]    NVARCHAR(MAX) NOT NULL,
 [Header]		NVARCHAR(50) NOT NULL,
 [Photo]		VARBINARY(MAX) NOT NULL,
 [PhotoExtension] NVARCHAR(10) NOT NULL,
 [Phone]		NVARCHAR(35) NOT NULL,
 [Address]		NVARCHAR(50) NOT NULL,
 [Site]			NVARCHAR(50) NOT NULL,
 
 CONSTRAINT  PK_tbltblHotelDescription_HotelId PRIMARY KEY([HotelId]),
 CONSTRAINT  FK_tblHotelDescriptions_HotelId_tblHotel_Id FOREIGN KEY(HotelId) REFERENCES tblHotel(Id) ON DELETE CASCADE ON UPDATE CASCADE,
)

GO

CREATE FUNCTION fnIsServiceAvailable(
@hotelRoomsId INT,
@service NVARCHAR(20))
RETURNS BIT
AS
BEGIN
	declare @hotelId as int;
	set @hotelId = (SELECT HotelId FROM tblHotelRooms WHERE Id = @hotelRoomsId);
	IF (@service = 'Parking' or @service = '[Parking]')
		IF EXISTS (SELECT *	FROM tblHotel WHERE Id = @hotelId AND [Parking] = 1)
			RETURN 1 
	IF (@service = 'Pool' or @service = '[Pool]')
		IF EXISTS (SELECT *	FROM tblHotel WHERE Id = @hotelId AND [Pool] = 1)
			RETURN 1 
	IF (@service = 'Sauna' or @service = '[Sauna]')
		IF EXISTS (SELECT *	FROM tblHotel WHERE Id = @hotelId AND [Sauna] = 1)
			RETURN 1 
	IF (@service = 'TennisCourt' or @service = '[TennisCourt]')
		IF EXISTS (SELECT *	FROM tblHotel WHERE Id = @hotelId AND [TennisCourt] = 1)
			RETURN 1 
	IF (@service = 'Gym' or @service = '[Gym]')
		IF EXISTS (SELECT *	FROM tblHotel WHERE Id = @hotelId AND [Gym] = 1)
			RETURN 1 
	RETURN 0
END

GO

CREATE TABLE dbo.tblHotelReservation
(
	[Id]        INT IDENTITY(1, 1) NOT NULL,
	[UserId]     INT NOT NULL,
    [HotelRoomsId] INT NOT NULL,
	[BeginDate]	Date NOT NULL,
	[EndDate]	Date NOT NULL,
	[Parking]   BIT NOT NULL,
	[Pool]      BIT NOT NULL,
	[Sauna]     BIT NOT NULL,
	[TennisCourt] BIT NOT NULL,
	[Gym]       BIT NOT NULL,
	[Status]       INT NOT NULL
	
	CONSTRAINT  PK_tblHotelReservation_Id PRIMARY KEY(Id),
	CONSTRAINT  FK_tblHotelReservation_UserId_tblUser_Id FOREIGN KEY(UserId) REFERENCES tblUser(Id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT  FK_tblHotelReservation_HotelRoomsId_tblHotelRooms_Id FOREIGN KEY(HotelRoomsId) REFERENCES tblHotelRooms(Id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT  CK_tblHotelReservation_EndDate CHECK (DATEDIFF(dd, [BeginDate], [EndDate]) >= 1),
	CONSTRAINT  CK_tblHotelReservation_Parking CHECK (Parking = 0 OR Parking = dbo.fnIsServiceAvailable([HotelRoomsId], 'Parking')),
	CONSTRAINT  CK_tblHotelReservation_Pool CHECK ([Pool] = 0 OR [Pool] = dbo.fnIsServiceAvailable([HotelRoomsId], 'Pool')),
	CONSTRAINT  CK_tblHotelReservation_Sauna CHECK ([Sauna] = 0 OR [Sauna] = dbo.fnIsServiceAvailable([HotelRoomsId], 'Sauna')),
	CONSTRAINT  CK_tblHotelReservation_TennisCourt CHECK ([TennisCourt] = 0 OR [TennisCourt] = dbo.fnIsServiceAvailable([HotelRoomsId], 'TennisCourt')),
	CONSTRAINT  CK_tblHotelReservation_Gym CHECK ([Gym] = 0 OR [Gym] = dbo.fnIsServiceAvailable([HotelRoomsId], 'Gym')),
	CONSTRAINT  CK_tblHotelReservation_Status CHECK ([Status] = 0 OR [Status] = 1 OR [Status] = 2),
	CONSTRAINT  CK_tblHotelReservation_MaxEndDate CHECK (DATEADD(year,1,GETDATE()) > [EndDate])

)

GO

CREATE TABLE dbo.tblUserFeedback 
( 
	Id          INT IDENTITY(1, 1) NOT NULL, 
	[Login]     NVARCHAR(16) NOT NULL, 
	Email       NVARCHAR(64) NOT NULL, 
	[Message]	NVARCHAR(4000) NOT NULL

	CONSTRAINT  PK_tblUserFeedback_Id PRIMARY KEY(Id),
	CONSTRAINT  CK_tblUserFeedback_Email CHECK (Email LIKE '%@%.%')
)

GO

CREATE FUNCTION  fnIsHotelAdmin(
@userId INT
)
RETURNS BIT
AS
BEGIN
	IF (EXISTS (SELECT * FROM tblUserRoles WHERE UserId = @userId AND RoleId = 3 ))
		RETURN 1
	RETURN 0
END

GO

CREATE TABLE dbo.tblHotelAdministrators
(
	Id INT IDENTITY(1, 1) NOT NULL, 
	UserId INT NOT NULL,
	HotelId INT NOT NULL

	
	CONSTRAINT FK_tblHotelAdministrators_UserId_tblUser_Id FOREIGN KEY(UserId) REFERENCES tblUser(Id) ON DELETE CASCADE ON UPDATE CASCADE, 
	CONSTRAINT FK_tblHotelAdministrators_HotelId_tblHotel_Id FOREIGN KEY(HotelId) REFERENCES tblHotel(Id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT UQ_tblHotelAdministrators_HotelId_UserType UNIQUE(HotelId, UserId),
	CONSTRAINT CK_tblHotelAdministrators_UserId_In_Role_HA CHECK (dbo.fnIsHotelAdmin(UserId) = 1)
)

CREATE TABLE dbo.tblExcursion
(
 [Id]			INT IDENTITY(1, 1) NOT NULL,
 [Name]			NVARCHAR(64) NOT NULL,
 [Address]		NVARCHAR(64) NOT NULL,
 [Duration]		TIME(0) NOT NULL,
 [BeginTime]    TIME(0) NOT NULL,
 [Price]		DECIMAL NOT NULL

 CONSTRAINT  PK_tblExcursion_Id PRIMARY KEY(Id)
)

CREATE TABLE dbo.tblExcursionDescription
(
 [ExcursionId]		 INT NOT NULL,
 [ShortDescription]    NVARCHAR(500) NOT NULL,
 [LongDescription]    NVARCHAR(MAX) NOT NULL,
 [Photo]		VARBINARY(MAX) NOT NULL,
 [PhotoExtension] NVARCHAR(10) NOT NULL,

 CONSTRAINT  PK_tblExcursionDescription_ExcursionId PRIMARY KEY([ExcursionId]),
 CONSTRAINT  FK_tblExcursionDescription_ExcursionId_tblExcursion_Id FOREIGN KEY([ExcursionId]) REFERENCES tblExcursion(Id) ON DELETE CASCADE ON UPDATE CASCADE,
)

CREATE TABLE dbo.tblExcursionReservation
(
	[Id]        INT IDENTITY(1, 1) NOT NULL,
	[UserId]     INT NOT NULL,
    [ExcursionId] INT NOT NULL,
	[ExcursionDate]	Date NOT NULL,
	[Status]		INT NOT NULL,
	
	CONSTRAINT  PK_tblExcursionReservation_Id PRIMARY KEY(Id),
	CONSTRAINT  FK_tblExcursionReservation_UserId_tblUser_Id FOREIGN KEY(UserId) REFERENCES tblUser(Id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT  FK_tblExcursionReservation_ExcursionId_tblExcursion_Id FOREIGN KEY([ExcursionId]) REFERENCES tblExcursion(Id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT  CK_tblExcursionReservation_ExcursionDate CHECK (DATEDIFF(dd, GETDATE(), [ExcursionDate]) >= 1),
	CONSTRAINT  CK_tblExcursionReservation_Status CHECK ([Status] = 0 OR [Status] = 1 OR [Status] = 2),
	CONSTRAINT  CK_tblExcursionReservation_MaxExcursionDate CHECK (DATEADD(day,7,GETDATE()) > [ExcursionDate])
)

GO

CREATE FUNCTION  fnIsExcursionAdmin(
@userId INT
)
RETURNS BIT
AS
BEGIN
	IF (EXISTS (SELECT * FROM tblUserRoles WHERE UserId = @userId AND RoleId = 5 ))
		RETURN 1
	RETURN 0
END

GO

CREATE TABLE dbo.tblExcursionAdministrators
(
	[Id]			INT	IDENTITY(1, 1) NOT NULL, 
	[UserId]		INT NOT NULL,
	[ExcursionId]	INT NOT NULL

	
	CONSTRAINT FK_tblExcursionAdministrators_UserId_tblUser_Id FOREIGN KEY(UserId) REFERENCES tblUser(Id) ON DELETE CASCADE ON UPDATE CASCADE, 
	CONSTRAINT FK_tblExcursionAdministrators_ExcursionId_tblExcursion_Id FOREIGN KEY(ExcursionId) REFERENCES tblExcursion(Id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT UQ_tblExcursionAdministrators_HotelId_UserType UNIQUE(ExcursionId, UserId),
	CONSTRAINT CK_tblExcursionAdministrators_UserId_In_Role_HA CHECK (dbo.fnIsExcursionAdmin(UserId) = 1)
)