USE CityCenter

GO

DROP TABLE tblExcursionAdministrators;
DROP TABLE tblExcursionReservation;
DROP TABLE tblExcursionDescription;
DROP TABLE tblExcursion;
DROP TABLE tblUserFeedback;
DROP TABLE tblHotelAdministrators;
DROP TABLE tblHotelReservation;
DROP TABLE tblHotelDescription;
DROP TABLE tblHotelRooms;
DROP TABLE tblHotel;
DROP TABLE tblUnconfirmedRegistration;
DROP TABLE tblUserRoles;
DROP TABLE tblUser;
DROP TABLE tblRole;