﻿using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using CityCenter.Entities;
using CityCenter.Repositories.Abstract;
using CityCenter.WebUI.Abstract;

namespace CityCenter.WebUI
{
    public class SecurityManager : ISecurityManager
    {
        #region Private Fields

        private readonly IUserRepository _userRepository;

        #endregion

        #region Constructors

        public SecurityManager(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        #endregion

        #region Interface Implementation

        public int GetCurrentUserId()
        {
            var currentUser = GetCurrentUser();
            return _userRepository.GetUserByLogin(currentUser).Id;
        }

        public string GetCurrentUser()
        {
            var currentUser = HttpContext.Current.User;
            var user = currentUser.Identity.Name;
            return user;
        }

        public bool Authentication(string login, string password)
        {
            var user = _userRepository.GetUserByLogin(login) ?? _userRepository.GetUserByEmail(login);
            if (user != null && user.Status &&
                CheckPassword(user.Password, password))
            {
                FormsAuthentication.SetAuthCookie(user.Login, false);
                return true;
            }

            return false;
        }

        public bool IsAuthenticated()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }

        public bool IsUserInRole(string role)
        {
            return HttpContext.Current.User.IsInRole(role);
        }

        public bool IsUserAdministratorOfHotel(int userId, int hotelId)
        {
            var res = _userRepository.IsUserAdministratorOfHotel(userId, hotelId);
            return res > 0;
        }

        public bool IsUserAdministratorOfExcursion(int userId, int excursionId)
        {
            var res = _userRepository.IsUserAdministratorOfExcursion(userId, excursionId);
            return res > 0;
        }


        public string ReadUserRole(string login, out bool status)
        {
            var user = _userRepository.GetUserByLogin(login);
            var rolesList = _userRepository.GetUserRoles(user.Id);

            status = user.Status;
            return rolesList.Aggregate("", (current, role) => current + (role.Name + ";"));
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }

        public int RegisterNewUser(User user)
        {
            if (IsPassValid(user.Password))
            {
                user.Password = EncryptPassword(user.Password);
                var token = GenerateToken(48);
                var dbUserId = _userRepository.InsertUser(user);
                _userRepository.InsertUnconfirmedRegistration(dbUserId, token, DateTime.Now);
                user.Status = false;
                return dbUserId;
            }
            return 0;
        }

        public void PasswordRecovery(User user)
        {
            if (_userRepository.GetUserPasswordRecoveryToken(user.Id) == null)
            {
                var token = GenerateToken(48);
                _userRepository.InserPasswordRecoveryRequest(user.Id, token, DateTime.Now);
            }
        }

        public void UpdateUserPassword(User user)
        {
            user.Password = EncryptPassword(user.Password);
            _userRepository.UpdateUserPassword(user);
        }

        public bool CheckPassword(string hashedPass, string enteredPass)
        {
            enteredPass = EncryptPassword(enteredPass);
            return hashedPass == enteredPass;
        }

        #endregion

        #region Helpers

        private string GenerateToken(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();

            var result = new string(
                Enumerable.Repeat(chars, length)
                            .Select(s => s[random.Next(s.Length)])
                            .ToArray());

            return result;
        }

        private string EncryptPassword(string password)
        {
            var bytesArr = Encoding.Default.GetBytes(password);
            var mySha256 = SHA256.Create();
            var hashValue = mySha256.ComputeHash(bytesArr);

            return Encoding.Default.GetString(hashValue);
        }

        private bool IsPassValid(string password)
        {
            Regex rgx = new Regex(@"^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,30}$");
            return rgx.IsMatch(password);
        }

        #endregion


    }
}
