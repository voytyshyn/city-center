﻿using System.Configuration;
using System.Web;
using CityCenter.BLL;
using CityCenter.BLL.Abstract;
using CityCenter.Repositories.Abstract;
using CityCenter.Repositories.Sql;
using CityCenter.WebUI.Abstract;
using Microsoft.Practices.Unity;
using NLog;

namespace CityCenter.WebUI.Admin.Code.DIP
{
    public class UnityConfig
    {
        private static IUnityContainer _container;

        public static void CreateContainer()
        {
            _container = new UnityContainer();
        }

        public static void RegisterServiceLocator()
        {
            HttpContext.Current.Application.Lock();
            try
            {
                HttpContext.Current.Application["ServiceLocator"] = new UnityServiceLocator(_container);
            }
            finally
            {
                HttpContext.Current.Application.UnLock();
            }
        }

        public static void RegisterComponents()
        {
            var connstr = ConfigurationManager.ConnectionStrings["CityCenter"].ConnectionString;

            _container.RegisterType<IUsersManager, UsersManager>();
            _container.RegisterType<IUserRepository, UserRepository>(
                new InjectionConstructor(connstr)
                );
            _container.RegisterType<ISecurityManager, SecurityManager>();
            _container.RegisterType<ILogger, Logger>(new InjectionFactory(f => LogManager.GetCurrentClassLogger()));
        }

        public static void DisposeContainer()
        {
            _container.Dispose();
        }
    }
}