﻿namespace CityCenter.WebUI.Admin.Code.Keys
{
    public static class SessionKeys
    {
        public const string USER_REPOSITORY_SESSION_KEY = "Repository";

        public const string BIND_METHOD = "Bind";
    }
}