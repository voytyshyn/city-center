﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CityCenter.WebUI.Admin.Login"  Theme="Login"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
     <link rel="shortcut icon" href="App_Themes/Default/Images/inner-logo.ico" type="image/x-icon" />
</head>

<body class="top">
<section id = "admin-panel">
	<div id = "header">
	Admin Panel
	</div>
	<div class="border">
	</div>
	 <form id="form1" runat="server">
        <div>
            <asp:Login ID="loginForm" TitleText="" runat="server" Font-Size="18px" OnAuthenticate="loginForm_Authenticate" PasswordLabelText="" FailureText="Invalid username or password" UserNameLabelText="" LoginButtonText="Log in" PasswordRequiredErrorMessage="Enter Password" RememberMeText="Remember me" UserNameRequiredErrorMessage="Enter login">
              </asp:Login>
        </div>
    </form>
	<div class = "border">
	</div>
</section>  
</body>
</html>
