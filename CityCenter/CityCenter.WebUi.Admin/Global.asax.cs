﻿using System;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Admin.Code;
using CityCenter.WebUI.Admin.Code.DIP;
using NLog;

namespace CityCenter.WebUI.Admin
{
    public class Global : HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            UnityConfig.CreateContainer();
            UnityConfig.RegisterServiceLocator();
            UnityConfig.RegisterComponents();
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            if (FormsAuthentication.CookiesSupported)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        var username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;

                        var securityManager = Application.GetServiceLocator().Resolve<ISecurityManager>();

                        bool status;
                        var roles = securityManager.ReadUserRole(username, out status);

                        if (!status)
                        {
                            securityManager.SignOut();
                        }
                        else
                        {
                            HttpContext.Current.User = new GenericPrincipal(
                              new GenericIdentity(username, "Forms"), roles.Split(';'));
                        }
                    }
                    catch (Exception ex)
                    {
                        var logger = Application.GetServiceLocator().Resolve<ILogger>();
                        logger.Fatal(ex);
                    }
                }
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                if (Server.GetLastError() == null) return;
                var ex = Server.GetLastError().GetBaseException();

                if (ex is HttpException == false)
                {
                    var logger = Application.GetServiceLocator().Resolve<ILogger>();
                    logger.Error(ex);
                }
                Server.Transfer("~/Error.aspx", true);
            }
            catch (Exception ex)
            {
                var logger = Application.GetServiceLocator().Resolve<ILogger>();
                logger.Fatal(ex);
            }
        }

        protected void Application_End(object sender, EventArgs e)
        {
            UnityConfig.DisposeContainer();
        }
    }
}