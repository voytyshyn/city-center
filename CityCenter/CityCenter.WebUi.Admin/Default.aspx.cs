﻿using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Admin.Code;
using CityCenter.WebUI.Admin.Code.DIP;

namespace CityCenter.WebUI.Admin
{
    public partial class Login : Page
    {
        #region Fields

        private ISecurityManager _securityManager;

        #endregion
        
        protected void Page_Load(object sender, EventArgs e)
        {
            IServiceLocator serviceLocator = Application.GetServiceLocator();
            _securityManager = serviceLocator.Resolve<ISecurityManager>();
            if (!IsPostBack)
            {
                if (Page.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.SignOut();
                    Response.Redirect("~/Default.aspx");
                }
            }
        }

        protected void loginForm_Authenticate(object sender, AuthenticateEventArgs e)
        {
            var userLogin = loginForm.UserName;
            var userPassword = loginForm.Password;

            if (_securityManager.Authentication(userLogin, userPassword))
            {
                var returnUrl = Request.QueryString["ReturnUrl"] ?? "~/AdminPage.aspx";
                Response.Redirect(returnUrl);
            }

        }
    }
}