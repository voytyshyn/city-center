﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using CityCenter.WebUI.Frontend.Controllers;
using CityCenter.WebUI.Frontend.Models.Hotel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PagedList;

namespace CityCenter.WebUI.Frontend.Tests
{
    [TestClass]
    public class HotelsControllerTestMock
    {
        #region Const fields

        private const int TOTAL_HOTEL_DESCRIPTIONS = 3;

        private const int PAGE_SIZE = 1;

        #endregion

        #region Private Fields

        private Mock<IHotelsManager> _mockHotelsManager;
        private HotelsController _controller;

        private List<HotelDescription> _fakeHotelDescriptions;
        private List<Hotel> _fakeHotels;

        #endregion

        #region Initialize

        [TestInitialize]
        public void SetUp()
        {
            _fakeHotels = new List<Hotel>
            {
                new Hotel
                {
                    Id = 1,
                    Name = "FakeHotel1"
                }
            };

            _fakeHotelDescriptions = new List<HotelDescription>
            {
                new HotelDescription
                {
                    HotelId = _fakeHotels[0].Id,
                    Photo = new byte[] {123, 23, 32, 23},
                    PhotoExtension = "image/png"
                }
            };

            _mockHotelsManager = new Mock<IHotelsManager>();
            var hotelsManager = _mockHotelsManager.Object;

            _mockHotelsManager.Setup(m => m.GetHotelDescriptionsCount()).Returns(TOTAL_HOTEL_DESCRIPTIONS);
            _mockHotelsManager.Setup(m => m.GetHotelDescriptions(It.IsAny<int>(), It.IsAny<int>())).Returns(_fakeHotelDescriptions);
            _mockHotelsManager.Setup(m => m.GetHotelDescriptionById(It.IsAny<int>())).Returns(_fakeHotelDescriptions[0]);
            _mockHotelsManager.Setup(m => m.GetHotelById(It.IsAny<int>())).Returns(_fakeHotels[0]);
            _mockHotelsManager.Setup(
                m =>
                    m.BookRoom(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(),
                        It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Returns(1);

            _controller = new HotelsController(hotelsManager);
        }

        #endregion

        #region Test WebActions

        [TestMethod]
        public void Test_Hotels()
        {
            var actionResult = _controller.Hotels();
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.IsNotNull(result.Model);
            Assert.IsNull(result.View);
            var model = result.Model as StaticPagedList<HotelDescriptionModel>;
            Assert.AreEqual(_fakeHotels[0].Name, model[0].HotelName);
            Assert.AreEqual(_fakeHotels[0].Id, model[0].HotelDescription.HotelId);
        }

        [TestMethod]
        public void Test_GetHotelImage()
        {
            var fileContentResult = _controller.GetHotelImage(1);
            Assert.IsInstanceOfType(fileContentResult, typeof(FileContentResult));
            Assert.IsNotNull(fileContentResult.FileContents);
        }

        [TestMethod]
        public void Test_Hotels_InvalidPage_1()
        {
            var actionResult = _controller.Hotels(page: 10);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("Hotels", result.RouteValues["action"]);
            Assert.AreEqual((int)Math.Ceiling((double)TOTAL_HOTEL_DESCRIPTIONS / PAGE_SIZE), result.RouteValues["page"]);
            Assert.IsNull(result.RouteValues["controller"]);

        }

        [TestMethod]
        public void Test_Hotels_InvalidPage_2()
        {
            var actionResult = _controller.Hotels(page: -1);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("Hotels", result.RouteValues["action"]);
            Assert.IsNull(result.RouteValues["page"]);
            Assert.IsNull(result.RouteValues["controller"]);
        }

        #endregion
    }
}
