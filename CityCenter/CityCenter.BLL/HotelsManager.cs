﻿using System;
using System.Collections.Generic;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using CityCenter.Repositories.Abstract;

namespace CityCenter.BLL
{
    public class HotelsManager : IHotelsManager
    {
        #region Private Fields

        private readonly IHotelRepository _hotelRepository;

        #endregion

        #region Constructor

        public HotelsManager(IHotelRepository hotelRepository)
        {
            _hotelRepository = hotelRepository;
        }

        #endregion

        #region IHotelsManager implementation

        public int BookRoom(int userId, int hotelRoomsId, DateTime beginDate, DateTime endDate, bool parking, bool pool, bool sauna, bool tennisCourt, bool gym)
        {
            return isBookingDateValid(beginDate, endDate)
                ? _hotelRepository.BookRoom(userId, hotelRoomsId, beginDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), parking, pool, sauna, tennisCourt, gym, status: 0)
                : 0;
        }

        public List<HotelRooms> GetHotelRoomTypes(int hotelId)
        {
            return _hotelRepository.GetHotelRoomTypes(hotelId);
        }

        public List<HotelRooms> GetAvailableHotelRoomTypes(int hotelId, DateTime beginDate, DateTime endDate)
        {
            return (isBookingDateValid(beginDate, endDate))
                ? _hotelRepository.GetAvailableHotelRoomTypes(hotelId, beginDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"))
                : null;
        }

        public int GetHotelRoomsId(int hotelId, string roomType)
        {
            return _hotelRepository.GetHotelRoomsId(hotelId, roomType);
        }

        public List<Hotel> GetHotels()
        {
            return _hotelRepository.GetHotels();
        }

        public List<HotelDescription> GetHotelDescriptions(int pageNumber, int pageSize)
        {
            return _hotelRepository.GetHotelDescriptions(pageNumber, pageSize);
        }

        public void UpdateHotelDescription(HotelDescription hotelDescription)
        {
            _hotelRepository.UpdateHotelDescription(hotelDescription);
        }

        public void UpdateHotelName(string name, int id)
        {
            _hotelRepository.UpdateHotelName(name, id);
        }

        public HotelDescription GetHotelDescriptionById(int hotelId)
        {
            return _hotelRepository.GetHotelDescriptionById(hotelId);
        }

        public Hotel GetHotelById(int id)
        {
            return _hotelRepository.GetHotelById(id);
        }

        public Hotel GetHotelByHotelName(string nameHotel)
        {
            return _hotelRepository.GetHotelByHotelName(nameHotel);
        }

        public int GetHotelDescriptionsCount()
        {
            return _hotelRepository.GetHotelDescriptionsCount();
        }

        public int GetReservationCount(int userId)
        {
            return _hotelRepository.GetReservationCount(userId);
        }

        public List<HotelReservationDetails> GetHotelReservationDetails(int userId)
        {
            return _hotelRepository.GetHotelReservationsByUserId(userId);
        }

        public List<HotelReservationDetails> GetHotelReservationDetails(int userId, int pageNumber, int pageSize)
        {
            return _hotelRepository.GetHotelReservationsByUserId(userId, pageNumber, pageSize);
        }

        public void UpdateHotelRoom(HotelRooms room)
        {
            _hotelRepository.UpdateHotelRoom(room);
        }

        public int InsertHotelRoom(HotelRooms room)
        {
            return _hotelRepository.InsertHotelRoom(room);
        }

        public void DeletetHotelRoom(HotelRooms room)
        {
            _hotelRepository.DeletetHotelRoom(room);
        }

        public int AddNewHotel(int userId, Hotel hotel, List<HotelRooms> rooms, HotelDescription description)
        {
            hotel.MapCoords = "3242435";
            var hotelId = _hotelRepository.InsertHotel(hotel);
            description.HotelId = hotelId;
            _hotelRepository.InsertNewHotelAdmin(hotelId, userId);
            foreach (var room in rooms)
            {
                room.HotelId = hotelId;
                _hotelRepository.InsertHotelRoom(room);
            }
            _hotelRepository.InsertHotelDescription(description);
            return hotelId;
        }

        public List<Hotel> GetHotelsByAdminId(int adminId)
        {
            return _hotelRepository.GetHotelsByAdminId(adminId);
        }

        public List<HotelReservationDetails> GetHotelReservationByHotelId(int hotelId)
        {
            return _hotelRepository.GetHotelReservationByHotelId(hotelId);
        }

        public List<HotelReservationDetails> GetHotelReservationByHotelId(int hotelId, int pageNumber, int pageSize)
        {
            return _hotelRepository.GetHotelReservationByHotelId(hotelId, pageNumber, pageSize, status: 0);
        }

        public void UpdateHotelImage(HotelDescription hotelDescription)
        {
            _hotelRepository.UpdateHotelImage(hotelDescription);
        }

        public int GetPendingReservationCountByHotelId(int hotelId)
        {
            return _hotelRepository.GetPendingReservationCountByHotelId(hotelId, status: 0);
        }

        public void AcceptReservation(int reservationId)
        {
            _hotelRepository.ChangeReservationStatus(reservationId, status: 2);
        }

        public void DeclineReservation(int reservationId)
        {
            _hotelRepository.ChangeReservationStatus(reservationId, status: 1);
        }

        public List<HotelReservationDetails> GetTodayPendingReservations()
        {
            return _hotelRepository.GetTodayPendingReservations();
        }

        public List<HotelReservationDetails> GetPendingHotelReservationByHotelId(int hotelId, int pageNumber, int pageSize)
        {
            return _hotelRepository.GetPendingHotelReservationByHotelId(hotelId, pageNumber, pageSize, status: 0);
        }

        public int GetReservationCountByHotelId(int hotelId)
        {
            return _hotelRepository.GetReservationCountByHotelId(hotelId, status: 0);
        }

        #endregion

        #region Validators

        private bool isBookingDateValid(DateTime beginDate, DateTime endDate)
        {
            return endDate - beginDate >= new TimeSpan(0, 0, 1);
        }

        #endregion
    }
}
