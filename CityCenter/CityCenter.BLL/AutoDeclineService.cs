﻿using System;
using System.Threading.Tasks;
using CityCenter.BLL.Abstract;
using NLog;

namespace CityCenter.BLL
{
    public class AutoDeclineService : IAutoDeclineService
    {
        #region Const fields

        private const int HOURS = 00;
        private const int MINUTES = 00;

        #endregion

        #region Private Fields

        private readonly IHotelsManager _hotelsManager;
        private readonly IExcursionsManager _excursionsManager;
        private readonly IUsersManager _usersManager;
        private readonly IEmailManager _emailManager;
        private readonly ILogger _logger;

        #endregion

        #region Constructor

        public AutoDeclineService(IHotelsManager hotelsManager, IExcursionsManager excursionsManager, IUsersManager usersManager, IEmailManager emailManager, ILogger logger)
        {
            _hotelsManager = hotelsManager;
            _excursionsManager = excursionsManager;
            _usersManager = usersManager;
            _emailManager = emailManager;
            _logger = logger;
        }

        #endregion

        #region Methods

        public void StartService()
        {
            //int HOURS = DateTime.Now.Hour;
            //int MINUTES = DateTime.Now.Minute;

            var dateNow = DateTime.Now;
            var date = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, HOURS, MINUTES, 0);

            _logger.Info(" Decline service has been started");
            DeclineTodayPendingReservations();

            date = GetNextDate(date);
            RunCodeAt(date);
            _logger.Info(" \tNext start at {0} {1}. In {2} from now", date.ToLongTimeString(), date.ToLongDateString(), date - DateTime.Now);
        }

        private void RunCodeAt(DateTime date)
        {
            var dateNow = DateTime.Now;
            TimeSpan ts;
            if (date > dateNow)
            {
                ts = date - dateNow;
            }
            else
            {
                _logger.Info(" Something went wrong, current date : {0} is less than next date to run : {1}. Generate new next date", dateNow, date);
                date = GetNextDate(date);
                _logger.Info(" New next date is : {0}", date);
                ts = date - dateNow;
            }

            Task.Delay(ts).ContinueWith(x =>
            {
                DeclineTodayPendingReservations();

                date = GetNextDate(date);
                RunCodeAt(date);
                _logger.Info(" \tNext start at {0} {1}. In {2} from now", date.ToLongTimeString(), date.ToLongDateString(), date - DateTime.Now);
            });
        }

        private void DeclineTodayPendingReservations()
        {
            try
            {
                _logger.Info(" DeclineTodayPendingReservations method has been started");
                var excursionReservationsToDecline = _excursionsManager.GetTodayPendingReservations();
                _logger.Info(" \tNumber of excursions reservation to decline : {0}", excursionReservationsToDecline.Count);
                foreach (var reservation in excursionReservationsToDecline)
                {
                    _excursionsManager.DeclineReservation(reservation.ReservationId);
                    _logger.Info(" \t\tExcursion reservation(id : {0} ) was declined", reservation.ReservationId);
                    var user = _usersManager.GetUserById(reservation.UserId);
                    _emailManager.SendExcursionReservationNotification(user, reservation);
                }

                var hotelReservationsToDecline = _hotelsManager.GetTodayPendingReservations();
                _logger.Info(" \tNumber of hotel reservation to decline : {0}", hotelReservationsToDecline.Count);
                foreach (var reservation in hotelReservationsToDecline)
                {
                    _hotelsManager.DeclineReservation(reservation.Id);
                    _logger.Info(" \t\tHotel reservation(id : {0} ) was declined", reservation.Id);
                    var userId = _usersManager.GetUserIdByReservationId(reservation.Id);
                    var user = _usersManager.GetUserById(userId);
                    _emailManager.SendHotelReservationNotification(user, reservation);
                }
                _logger.Info(" DeclineTodayPendingReservations method has been ended");
            }
            catch (Exception ex)
            {
                _logger.Error(" {0}", ex.Message);
            }
        }

        private DateTime GetNextDate(DateTime date)
        {
            return date.AddDays(1);
        }

        #endregion
    }
}
