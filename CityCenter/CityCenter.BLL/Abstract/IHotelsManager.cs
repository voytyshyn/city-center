﻿using System;
using System.Collections.Generic;
using CityCenter.Entities;

namespace CityCenter.BLL.Abstract
{
    public interface IHotelsManager
    {
        int BookRoom(int userId, int hotelRoomsId, DateTime beginDate, DateTime endDate, bool parking, bool pool, bool sauna, bool tennisCourt, bool gym);

        List<HotelRooms> GetHotelRoomTypes(int hotelId);

        List<HotelRooms> GetAvailableHotelRoomTypes(int hotelId, DateTime beginDate, DateTime endDate);

        int GetHotelRoomsId(int hotelId, string roomType);

        List<Hotel> GetHotels();

        List<HotelDescription> GetHotelDescriptions(int pageNumber, int pageSize);

        void UpdateHotelDescription(HotelDescription hotelDescription);

        Hotel GetHotelById(int id);

        HotelDescription GetHotelDescriptionById(int hotelId);

        int GetHotelDescriptionsCount();

        int GetReservationCount(int userId);

        List<HotelReservationDetails> GetHotelReservationDetails(int userId);

        List<HotelReservationDetails> GetHotelReservationDetails(int userId, int pageNumber, int pageSize);
    
		void UpdateHotelRoom(HotelRooms room);

        int InsertHotelRoom(HotelRooms room);

        void DeletetHotelRoom(HotelRooms room);

        int AddNewHotel(int userId, Hotel hotel, List<HotelRooms> rooms, HotelDescription description);

        List<Hotel> GetHotelsByAdminId(int adminId);

        List<HotelReservationDetails> GetHotelReservationByHotelId(int hotelId);

        List<HotelReservationDetails> GetHotelReservationByHotelId(int hotelId, int pageNumber, int pageSize);

        List<HotelReservationDetails> GetPendingHotelReservationByHotelId(int hotelId, int pageNumber, int pageSize);

        Hotel GetHotelByHotelName(string nameHotel);

        void UpdateHotelName(string name, int id);

        void UpdateHotelImage(HotelDescription hotelDescription);

        int GetReservationCountByHotelId(int hotelId);

        int GetPendingReservationCountByHotelId(int hotelId);

        void AcceptReservation(int reservationId);

        void DeclineReservation(int reservationId);

        List<HotelReservationDetails> GetTodayPendingReservations();
    }
}
