﻿using System;
using System.Collections.Generic;
using CityCenter.Entities;

namespace CityCenter.BLL.Abstract
{
    public interface IExcursionsManager
    {
        int AddNewReservation(int userId, int excursionId, DateTime excursionDate);

        List<Excursion> GetExcursions();

        List<Excursion> GetExcursions(int pageNumber, int pageSize);

        List<ExcursionDescription> GetExcursionDescriptions();

        List<ExcursionDescription> GetExcursionDescriptions(int pageNumber, int pageSize);

        int GetExcursionDescriptionsCount();

        Excursion GetExcursionById(int id);

        Excursion GetExcursionByName(string name);

        ExcursionDescription GetExcursionDescriptionById(int excursionId);

        int GetReservationDetailsCountByUserId(int userId);

        List<ExcursionReservationDetails> GetReservationDetailsByUserId(int userId, int pageNumber, int pageSize);

        int GetAcceptedReservationsCountByExcursionId(int excursionId);

        List<ExcursionReservationDetails> GetAcceptedReservationsByExcursionId(int excursionId, int pageNumber, int pageSize);

        int GetPendingReservationsCountByExcursionId(int excursionId);

        List<ExcursionReservationDetails> GetPendingReservationsByExcursionId(int excursionId, int pageNumber, int pageSize);

        void UpdateExcursionImage(ExcursionDescription excursionDescription);

        void UpdateExcursionDescription(ExcursionDescription excursionDescription);

        void UpdateExcursionName(string name, int id);

        void AcceptReservation(int reservationId);

        void DeclineReservation(int reservationId);

        List<Excursion>GetExcursionsByAdminId(int adminId);

        int AddNewExcursion(Excursion excursion, ExcursionDescription description, int userId);

        List<ExcursionReservationDetails> GetTodayPendingReservations();
    }
}
